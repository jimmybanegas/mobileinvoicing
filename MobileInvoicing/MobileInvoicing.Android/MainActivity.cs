﻿using System.Globalization;
using System.Threading;
using Android.App;
using Android.Content.PM;
using Android.OS;


namespace MobileInvoicing.Droid
{
    [Activity(Label = "Facturacion", Icon = "@drawable/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);

            global::Xamarin.Forms.Forms.Init(this, bundle);
            ZXing.Net.Mobile.Forms.Android.Platform.Init();

            var userSelectedCulture = new CultureInfo("es-HN");

            Thread.CurrentThread.CurrentCulture = userSelectedCulture;

            LoadApplication(new App());
        }
        
    
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Permission[] grantResults)
        {
            ZXing.Net.Mobile.Forms.Android.PermissionsHandler.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        protected override void OnResume()
        {
            base.OnResume();

            // Here you would read it from where ever.
            var userSelectedCulture = new CultureInfo("es-HN");

            Thread.CurrentThread.CurrentCulture = userSelectedCulture;
        }
    }

}

