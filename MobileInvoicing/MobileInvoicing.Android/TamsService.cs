﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using MobileInvoicing.Data;
using MobileInvoicing.Droid.Helpers;
using MobileInvoicing.Models;
using Xamarin.Forms;

[assembly: Dependency(typeof(MobileInvoicing.Droid.TamsService))]
namespace MobileInvoicing.Droid
{
    class TamsService : ITamsService
    {
        private SqlConnectionStringBuilder _builder;
            //new SqlConnectionStringBuilder(Conections.GetTamsConectionString())
            //{
            //    AsynchronousProcessing = true
            //};

        public async Task<IEnumerable<Product>> GetProductsAsync()
        {
            try
            {
                _builder = new SqlConnectionStringBuilder(Conections.GetTamsConectionString())
                {
                    AsynchronousProcessing = true
                };

                //var sql = "SELECT A.codprod, A.nomprod,A.BARRA, B.LISTA1 AS PRECIO1,  B.LISTA2 AS PRECIO2, C.por_impto AS IMPUESTO , A.coduni " +
                //          "FROM ayp_maeprod as A JOIN ayp_listasdet AS B ON A.codprod = B.codprod JOIN ayp_impto AS c ON A.cod_impto = C.cod_impto WHERE B.SUCURSAL = 1\r\n";

                var sql = "SELECT A.codprod, A.nomprod, ISNULL(A.BARRA, ''), ISNULL(VENTA4,0) AS PRECIO1,  ISNULL(VENTA4,0) AS PRECIO2, C.por_impto AS IMPUESTO , A.coduni " +
                "FROM ayp_maeprod AS A JOIN ayp_impto AS c ON A.cod_impto = C.cod_impto \r\n";

                var products = new List<Product>();

                using (var cn = new SqlConnection(_builder.ConnectionString))
                {

                    await cn.OpenAsync();

                    var command = new SqlCommand(sql, cn);

                    using (command)
                    {
                        using (var reader = await command.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                for (var j = 0; j + 6 < reader.FieldCount; j++)
                                {
                                    products.Add(new Product
                                    {
                                        Code = reader[j].ToString().Trim(),
                                        Name = reader[j + 1].ToString(),
                                        Barcode = reader[j + 2].ToString().Trim() ?? " ",
                                        Price = (decimal)(reader[j + 3]),
                                        Price2 = (decimal)(reader[j + 4]),
                                        TaxPercentage = Convert.ToInt32(reader[j + 5]),
                                        Measure = reader[j + 6].ToString().Trim()
                                    });
                                }
                            }
                        }
                    }

                    cn.Close();
                }

                return products;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
         
        }

        public async Task<IEnumerable<Customer>> GetCustomersAsync()
        {
            _builder = new SqlConnectionStringBuilder(Conections.GetTamsConectionString())
            {
                AsynchronousProcessing = true
            };

            //var sql = "SELECT CLIENTE,NOMBRE,LEGAL,PRIORIDAD FROM AYP_clientes where agente = " + Settings.Vendor;
            var sql = "SELECT CLIENTE,NOMBRE,LEGAL,PRIORIDAD FROM AYP_clientes where cliente = 166";

            //Reutilizar la clase Note para guardar el resultado del query en ella
            var customers = new List<Customer>();

            using (var cn = new SqlConnection(_builder.ConnectionString))
            {
                await cn.OpenAsync();

                var command = new SqlCommand(sql, cn);

                using (command)
                {
                    using (var reader = await command.ExecuteReaderAsync())
                    {
                        while (await reader.ReadAsync())
                        {
                            for (var j = 0; j + 3 < reader.FieldCount; j++)
                            {
                                customers.Add(new Customer
                                {
                                    Code = Convert.ToInt32(reader[j]),
                                    Name = reader[j + 1].ToString().Trim(),
                                    RtnOrId = reader[j + 2].ToString().Trim(),
                                    Priority = reader[j + 3] is DBNull ? 0 : Convert.ToInt32(reader[j + 3])
                                });
                            }
                        }
                    }
                }

                cn.Close();
            }

            return customers;
        }

        public async Task<IEnumerable<Vendor>> GetVendorsAsync()
        {
            _builder = new SqlConnectionStringBuilder(Conections.GetTamsConectionString())
            {
                AsynchronousProcessing = true
            };

            var sql = "SELECT VENDEDOR,NOMVEN FROM AYP_vendedores";

            //Reutilizar la clase Note para guardar el resultado del query en ella
            var vendors = new List<Vendor>();

            using (var cn = new SqlConnection(_builder.ConnectionString))
            {
                await cn.OpenAsync();

                var command = new SqlCommand(sql, cn);

                using (command)
                {
                    using (var reader = await command.ExecuteReaderAsync())
                    {
                        while (await reader.ReadAsync())
                        {
                            for (var j = 0; j + 1 < reader.FieldCount; j++)
                            {
                                vendors.Add(new Vendor
                                {
                                    Code = Convert.ToInt32(reader[j]),
                                    Name = reader[j + 1].ToString().Trim()
                                });
                            }
                        }
                    }
                }

                cn.Close();
            }

            return vendors;
        }

        public async Task<bool> SendInvoices(ObservableCollection<Invoice> invoices)
        {
            _builder = new SqlConnectionStringBuilder(Conections.GetTamsConectionString())
            {
                AsynchronousProcessing = true
            };

            const string format = "yyyy-MM-dd HH:MM:ss";
            try
            {
                using (var cn = new SqlConnection(_builder.ConnectionString))
                {
                    await cn.OpenAsync();

                    foreach (var invoice in invoices)
                    {
                        string cmdFacturas;
                        if (invoice.IsVoid)
                        {
                            cmdFacturas =
                                "INSERT INTO FAC_PREFACTURAR( sucursal, factura, fecha, cliente, nombre, concepto, subtotalv, valdescto, valimpto, valimpto2, " +
                                "valimpto3, valimpto4, flete, isvflete, totalv, tasa, agente," +
                                "pedido, oproduc, condicion, fecven, diasplazo, monto, " +
                                "saldo_act, abonos, saldo_ant, pagada, abonada, ocompra, fecpago, fecabono, enviara, odespacho, observa," +
                                "usuario, fechayhora, MANUAL, CAJA, ANULADA, ANULADAPOR, FECHANULA, CONCEPTOANU, ENTREGA, comentario, ENTREGADO, ENTREGADOPOR," +
                                "FECHAENTREGA, fechar, cerrada, moneda, cai, fechalimite, establecimiento, puntoemision, tipodoctof, rtnid, gravada," +
                                " exenta, inicio, final, devolucion, notacred," +
                                $"montocred, isvcred) VALUES ('{4}','{invoice.InvoiceNumber}','{invoice.Date.ToString(format)}','{invoice.Customer.Code}','{invoice.Customer.Name.Trim()}','{" "}','{invoice.Subtotal}','{0}'," +
                                $"'{invoice.Tax}','{0}','{0}','{0}','{0}','{0}','{invoice.Total}','{1}','{invoice.Vendor.Code}','{""}','{""}','{1}','{invoice.Date.ToString(format)}'," +
                                $"'{1}','{invoice.Total}','{0}','{invoice.Total}','{invoice.Total}','{"S"}','{"S"}','{""}','{invoice.Date.ToString(format)}','{invoice.Date.ToString(format)}','{""}','{""}','{""}','{"SYSTEM"}','{invoice.Date.ToString(format)}'," +
                                $"'{"N"}','{0}','{"S"}','{""}','{DateTime.Now.ToString(format)}','{0}','{0}','{""}','{0}','{""}','{invoice.Date.ToString(format)}','{invoice.Date.ToString(format)}','{0}'," +
                                $"'{"LEMPIRAS"}','{invoice.Cai}','{invoice.LimitDate.ToString(format)}','{invoice.Stablishment}','{invoice.SalesPoint}','{1}','{invoice.Customer.RtnOrId.Trim()}','{invoice.SubTotalTax}','{invoice.SubTotalNoTax}','{invoice.BeginDocument}','{invoice.EndDocument}','{""}','{""}'," +
                                $"'{0}','{0}')";
                        }
                        else
                        {
                            cmdFacturas =
                               "INSERT INTO FAC_PREFACTURAR( sucursal, factura, fecha, cliente, nombre, concepto, subtotalv, valdescto, valimpto, valimpto2, " +
                               "valimpto3, valimpto4, flete, isvflete, totalv, tasa, agente," +
                               "pedido, oproduc, condicion, fecven, diasplazo, monto, " +
                               "saldo_act, abonos, saldo_ant, pagada, abonada, ocompra, fecpago, fecabono, enviara, odespacho, observa," +
                               "usuario, fechayhora, MANUAL, CAJA, ANULADA, ANULADAPOR, FECHANULA, CONCEPTOANU, ENTREGA, comentario, ENTREGADO, ENTREGADOPOR," +
                               "FECHAENTREGA, fechar, cerrada, moneda, cai, fechalimite, establecimiento, puntoemision, tipodoctof, rtnid, gravada," +
                               " exenta, inicio, final, devolucion, notacred," +
                               $"montocred, isvcred) VALUES ('{4}','{invoice.InvoiceNumber}','{invoice.Date.ToString(format)}','{invoice.Customer.Code}','{invoice.Customer.Name.Trim()}','{" "}','{invoice.Subtotal}','{0}'," +
                               $"'{invoice.Tax}','{0}','{0}','{0}','{0}','{0}','{invoice.Total}','{1}','{invoice.Vendor.Code}','{""}','{""}','{1}','{invoice.Date.ToString(format)}'," +
                               $"'{1}','{invoice.Total}','{0}','{invoice.Total}','{invoice.Total}','{"S"}','{"S"}','{""}','{invoice.Date.ToString(format)}','{invoice.Date.ToString(format)}','{""}','{""}','{""}','{"SYSTEM"}','{invoice.Date.ToString(format)}'," +
                               $"'{"N"}','{0}','{"N"}','{""}','{DateTime.Now.ToString(format)}','{0}','{0}','{""}','{0}','{""}','{invoice.Date.ToString(format)}','{invoice.Date.ToString(format)}','{0}'," +
                               $"'{"LEMPIRAS"}','{invoice.Cai}','{invoice.LimitDate.ToString(format)}','{invoice.Stablishment}','{invoice.SalesPoint}','{1}','{invoice.Customer.RtnOrId.Trim()}','{invoice.SubTotalTax}','{invoice.SubTotalNoTax}','{invoice.BeginDocument}','{invoice.EndDocument}','{""}','{""}'," +
                               $"'{0}','{0}')";
                        }
                       
                        using (var cmd = new SqlCommand(cmdFacturas, cn))
                        {
                            cmd.ExecuteNonQuery();
                        }
                        
                        // Getting details of the invoice
                        using (var data = new DataAccess())
                        {
                            invoice.Products = data.GetInvoiceDetails().Where(c => c.InvoiceId == invoice.Id).ToList();
                        }

                        foreach (var detail in invoice.GetProducts())
                        {

                            var cmdDetails = string.Format("INSERT INTO FAC_PREDETFAC (fila, factura, codprod, nomprod, medida, tamaño, serie, cantidad, preciou, lista, exenta, descuento," +
                                    "valor_subt, valor_des, valor_isv, valor_tot, autorizado, promocion, descto, impto, presen, espesor, sucursal, costoventa, coddesc," +
                                    "valor_im2, valor_im3, valor_im4, impto2, impto3, impto4) VALUES ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}'," +
                                    "'{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}','{18}','{19}','{20}'," +
                                    "'{21}','{22}','{23}','{24}','{25}','{26}','{27}','{28}','{29}','{30}')", detail.Id, detail.Invoice.InvoiceNumber, detail.Product.Code, detail.Product.Name.Trim(),
                                    detail.Product.Measure, 0, "", detail.Quantity, detail.Price, 0, 0, 0, detail.Subtotal, 0, detail.Tax, detail.Total, "", "N", 0, 0, "", "", 4, 0, 0, 0,0,0,0,0,0)  ;

                            using (var cmd = new SqlCommand(cmdDetails, cn))
                            {
                                cmd.ExecuteNonQuery();
                            }
                        }


                        var cmdPayment = "";

                        // Getting payment details of the invoice
                        using (var data = new DataAccess())
                        {
                            invoice.PaymentDetails = data.GetInvoicePaymentDetails().Where(c => c.InvoiceId == invoice.Id).ToList();
                        }

                        foreach (var payment in invoice.PaymentDetails)
                        {
                            switch (payment.PaymentMethod)
                            {
                                case 1:
                                    cmdPayment = string.Format("INSERT INTO FAC_PREDETFORMAP (factura, sucursal, formap, desforma, comenta1, comenta2, valor, cbanco, reciboant, caja, cajero ) " +
                                                               "VALUES  ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}', '{8}','{9}','{10}')",
                                                               invoice.InvoiceNumber, 4, 1, "EFECTIVO", "", "", payment.Total, 0, "", "", "");
                                    break;
                                case 2:
                                    cmdPayment = string.Format("INSERT INTO FAC_PREDETFORMAP (factura, sucursal, formap, desforma, comenta1, comenta2, valor, cbanco, reciboant, caja, cajero ) " +
                                                               "VALUES  ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}', '{8}','{9}','{10}')",
                                        invoice.InvoiceNumber, 4, 2, "CHEQUE", "", "", payment.Total, 0, "", "", "");
                                    break;
                                case 3:
                                    cmdPayment = string.Format("INSERT INTO FAC_PREDETFORMAP (factura, sucursal, formap, desforma, comenta1, comenta2, valor, cbanco, reciboant, caja, cajero ) " +
                                                               "VALUES  ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}', '{8}','{9}','{10}')",
                                        invoice.InvoiceNumber, 4, 3, "CHEQUE CERTIFICADO", "", "", payment.Total, 0, "", "", "");
                                    break;
                                case 4:
                                    cmdPayment = string.Format("INSERT INTO FAC_PREDETFORMAP (factura, sucursal, formap, desforma, comenta1, comenta2, valor, cbanco, reciboant, caja, cajero ) " +
                                                               "VALUES  ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}', '{8}','{9}','{10}')",
                                        invoice.InvoiceNumber, 4, 4, "TARJETA DE CREDITO", "", "", payment.Total, 0, "", "", "");
                                    break;
                                case 5:
                                    cmdPayment = string.Format("INSERT INTO FAC_PREDETFORMAP (factura, sucursal, formap, desforma, comenta1, comenta2, valor, cbanco, reciboant, caja, cajero ) " +
                                                               "VALUES  ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}', '{8}','{9}','{10}')",
                                        invoice.InvoiceNumber, 4, 5, "TRANSFERENCIAS BANCARIAS", "", "", payment.Total, 0, "", "", "");
                                    break;
                            }

                            using (var cmd = new SqlCommand(cmdPayment, cn))
                            {
                                cmd.ExecuteNonQuery();
                            }
                        }
                    }

                    cn.Close();
                }

                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
            
        }

        public async Task<bool> SendOrders(ObservableCollection<Order> orders)
        {
            _builder = new SqlConnectionStringBuilder(Conections.GetTamsConectionString())
            {
                AsynchronousProcessing = true
            };

            const string format = "yyyy-MM-dd HH:MM:ss";
            try
            {
                using (var cn = new SqlConnection(_builder.ConnectionString))
                {
                    await cn.OpenAsync();

                    foreach (var order in orders)
                    {
                        string cmdOrders;
                        if (order.IsVoid)
                        {
                            cmdOrders =
                                "INSERT INTO FAC_PEDIDOS( sucursal, Pedido, fecha, cliente, nombre, concepto, subtotalv, valdescto, valimpto, valimpto2, " +
                                "valimpto3, valimpto4, flete, isvflete, totalv, tasa, agente," +
                                "oproduc, condicion, fecven, diasplazo, monto, " +
                                "saldo_act, abonos, saldo_ant, pagada, abonada, ocompra, fecpago, fecabono, enviara, odespacho, observa," +
                                "usuario, fechayhora, MANUAL, CAJA, ANULADA, ANULADAPOR, FECHANULA, CONCEPTOANU, ENTREGA, comentario, ENTREGADO, ENTREGADOPOR," +
                                "FECHAENTREGA, fechar, cerrada, moneda, rtnid, gravada," +
                                " exenta, devolucion, notacred," +
                                $"montocred, isvcred) VALUES ('{order.SalesPoint}','{order.Id.ToString()}','{order.Date.ToString(format)}','{order.Customer.Code}','{order.Customer.Name.Trim()}','{" "}','{order.Subtotal}','{0}'," +
                                $"'{order.Tax}','{0}','{0}','{0}','{0}','{0}','{order.Total}','{1}','{order.Vendor.Code}','{""}','{1}','{order.Date.ToString(format)}'," +
                                $"'{1}','{order.Total}','{0}','{order.Total}','{order.Total}','{"S"}','{"S"}','{""}','{order.Date.ToString(format)}','{order.Date.ToString(format)}','{""}','{""}','{""}','{"SYSTEM"}','{order.Date.ToString(format)}'," +
                                $"'{"N"}','{0}','{"S"}','{""}','{DateTime.Now.ToString(format)}','{0}','{0}','{""}','{0}','{""}','{order.Date.ToString(format)}','{order.Date.ToString(format)}','{0}'," +
                                $"'{"LEMPIRAS"}','{order.Customer.RtnOrId.Trim()}','{order.SubTotalTax}','{order.SubTotalNoTax}','{""}','{""}'," +
                                $"'{0}','{0}')";
                        }
                        else
                        {
                            cmdOrders =
                               "INSERT INTO FAC_PEDIDOS( sucursal, Pedido, fecha, cliente, nombre, concepto, subtotalv, valdescto, valimpto, valimpto2, " +
                               "valimpto3, valimpto4, flete, isvflete, totalv, tasa, agente," +
                               "oproduc, condicion, fecven, diasplazo, monto, " +
                               "saldo_act, abonos, saldo_ant, pagada, abonada, ocompra, fecpago, fecabono, enviara, odespacho, observa," +
                               "usuario, fechayhora, MANUAL, CAJA, ANULADA, ANULADAPOR, FECHANULA, CONCEPTOANU, ENTREGA, comentario, ENTREGADO, ENTREGADOPOR," +
                               "FECHAENTREGA, fechar, cerrada, moneda, rtnid, gravada," +
                               " exenta, devolucion, notacred," +
                               $"montocred, isvcred) VALUES ('{order.SalesPoint}','{order.Id.ToString()}','{order.Date.ToString(format)}','{order.Customer.Code}','{order.Customer.Name.Trim()}','{" "}','{order.Subtotal}','{0}'," +
                               $"'{order.Tax}','{0}','{0}','{0}','{0}','{0}','{order.Total}','{1}','{order.Vendor.Code}','{""}','{1}','{order.Date.ToString(format)}'," +
                               $"'{1}','{order.Total}','{0}','{order.Total}','{order.Total}','{"S"}','{"S"}','{""}','{order.Date.ToString(format)}','{order.Date.ToString(format)}','{""}','{""}','{""}','{"SYSTEM"}','{order.Date.ToString(format)}'," +
                               $"'{"N"}','{0}','{"N"}','{""}','{DateTime.Now.ToString(format)}','{0}','{0}','{""}','{0}','{""}','{order.Date.ToString(format)}','{order.Date.ToString(format)}','{0}'," +
                               $"'{"LEMPIRAS"}','{order.Customer.RtnOrId.Trim()}','{order.SubTotalTax}','{order.SubTotalNoTax}','{""}','{""}'," +
                               $"'{0}','{0}')";
                        }

                        using (var cmd = new SqlCommand(cmdOrders, cn))
                        {
                            cmd.ExecuteNonQuery();
                        }

                        // Getting details of the invoice
                        using (var data = new DataAccess())
                        {
                            order.Products = data.GetOrderDetails().Where(c => c.OrderId == order.Id).ToList();
                        }

                        foreach (var detail in order.GetProducts())
                        {

                            var cmdDetails = string.Format("INSERT INTO FAC_DETPED (fila, pedido, codprod, nomprod, medida, tamaño, serie, cantidad, preciou, lista, exenta, descuento," +
                                    "valor_subt, valor_des, valor_isv, valor_tot, autorizado, promocion, descto, impto, presen, espesor, sucursal, costoventa, coddesc," +
                                    "valor_im2, valor_im3, valor_im4, impto2, impto3, impto4) VALUES ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}'," +
                                    "'{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}','{18}','{19}','{20}'," +
                                    "'{21}','{22}','{23}','{24}','{25}','{26}','{27}','{28}','{29}','{30}')", detail.Id, detail.Order.Id, detail.Product.Code, detail.Product.Name.Trim(),
                                    detail.Product.Measure, 0, "", detail.Quantity, detail.Price, 0, 0, 0, detail.Subtotal, 0, detail.Tax, detail.Total, "", "N", 0, 0, "", "", 4, 0, 0, 0, 0, 0, 0, 0, 0);

                            using (var cmd = new SqlCommand(cmdDetails, cn))
                            {
                                cmd.ExecuteNonQuery();
                            }
                        }

                    }

                    cn.Close();
                }

                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
    }
}