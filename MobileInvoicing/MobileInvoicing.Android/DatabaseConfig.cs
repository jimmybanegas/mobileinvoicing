﻿using MobileInvoicing.Data;
using SQLite.Net.Interop;
using Xamarin.Forms;

[assembly: Dependency(typeof(MobileInvoicing.Droid.DatabaseConfig))]
namespace MobileInvoicing.Droid
{
    class DatabaseConfig : IDataBase
    {
        private string _dbDirectory;
        private ISQLitePlatform _platform;

        public string DatabaseDirectory
        {
            get
            {
                if (string.IsNullOrEmpty(_dbDirectory))
                {
                    _dbDirectory = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                }
                return _dbDirectory;
            }
        }

        public ISQLitePlatform Plataform
        {
            get
            {
                if (_platform == null)
                {
                    _platform = new SQLite.Net.Platform.XamarinAndroid.SQLitePlatformAndroidN();
                }
                return _platform;
            }
        }
    }
}