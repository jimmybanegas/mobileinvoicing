﻿using System;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using MobileInvoicing.Droid;
using MobileInvoicing.Helpers;
using MobileInvoicing.Models;
using Xamarin.Forms;

[assembly: Xamarin.Forms.Dependency(typeof(DroidMethods))]
namespace MobileInvoicing.Droid
{
    public class DroidMethods : Java.Lang.Object, INativeMethods
    {
        public Task ShowDialog(InvoiceDetail _item, string _description)
        {
            var tcs = new TaskCompletionSource<bool>();

            var builder = new AlertDialog.Builder(Forms.Context);
            var inflater = Forms.Context.GetSystemService(Context.LayoutInflaterService) as LayoutInflater;
            var dialogView = inflater.Inflate(Resource.Layout.DroidContentDialog, null);
            AlertDialog dialog = null;
            builder.SetView(dialogView);

            builder.SetTitle(_description);

            builder.SetCancelable(true);

            //EditText txtQuantity = ((EditText)dialogView.FindViewById(Resource.Id.quantity));
            //txtQuantity.KeyPress += (sender, e) =>
            //{
            //    if (e.KeyCode == Keycode.Enter)
            //    {
            //        if (txtQuantity.Text.Trim().Length > 0)
            //            _item.Quantity = Convert.ToInt32(txtQuantity.Text);

            //        dialog.Dismiss();
            //    }
            //};

            var txtQuantity = ((EditText)dialogView.FindViewById(Resource.Id.quantity));
            txtQuantity.AfterTextChanged += (sender, args) =>
            {
                int.TryParse(txtQuantity.Text, out var result);
                //amount = result;

                //    {
                if (result > 0)
                    _item.Quantity = result;

             //   dialog.Dismiss();
                //    }
            };

            // Add change button
            builder.SetPositiveButton(Android.Resource.String.Ok, (sender, e) =>
            {
                int.TryParse(txtQuantity.Text, out var result);
                if (result > 0)
                    _item.Quantity = Convert.ToInt32(txtQuantity.Text);

                tcs.SetResult(true);
            });


            // Add cancel button
            builder.SetNegativeButton(Android.Resource.String.Cancel, (sender, e) =>
            {
                tcs.SetResult(false);
            });

            dialog = builder.Create();

            dialog.DismissEvent += (sender, e) =>
            {
                //just in case the user pressed the back button or press outside dialog
                tcs.TrySetResult(false);
            };

            dialog.Show();

            return tcs.Task;
        }

        public Task ShowOrderDialog(OrderDetail _item, string _description)
        {
            var tcs = new TaskCompletionSource<bool>();

            var builder = new AlertDialog.Builder(Forms.Context);
            var inflater = Forms.Context.GetSystemService(Context.LayoutInflaterService) as LayoutInflater;
            var dialogView = inflater.Inflate(Resource.Layout.DroidContentDialog, null);
            AlertDialog dialog = null;
            builder.SetView(dialogView);

            builder.SetTitle(_description);

            builder.SetCancelable(true);

            //EditText txtQuantity = ((EditText)dialogView.FindViewById(Resource.Id.quantity));
            //txtQuantity.KeyPress += (sender, e) =>
            //{
            //    if (e.KeyCode == Keycode.Enter)
            //    {
            //        if (txtQuantity.Text.Trim().Length > 0)
            //            _item.Quantity = Convert.ToInt32(txtQuantity.Text);

            //        dialog.Dismiss();
            //    }
            //};

            var txtQuantity = ((EditText)dialogView.FindViewById(Resource.Id.quantity));
            txtQuantity.AfterTextChanged += (sender, args) =>
            {
                int.TryParse(txtQuantity.Text, out var result);
                //amount = result;

                //    {
                if (result > 0)
                    _item.Quantity = result;

                //   dialog.Dismiss();
                //    }
            };

            // Add change button
            builder.SetPositiveButton(Android.Resource.String.Ok, (sender, e) =>
            {
                int.TryParse(txtQuantity.Text, out var result);
                if (result > 0)
                    _item.Quantity = Convert.ToInt32(txtQuantity.Text);

                tcs.SetResult(true);
            });


            // Add cancel button
            builder.SetNegativeButton(Android.Resource.String.Cancel, (sender, e) =>
            {
                tcs.SetResult(false);
            });

            dialog = builder.Create();

            dialog.DismissEvent += (sender, e) =>
            {
                //just in case the user pressed the back button or press outside dialog
                tcs.TrySetResult(false);
            };

            dialog.Show();

            return tcs.Task;
        }

        public void SetStatusBar(string _backgroundHexColor)
        {
            //Coloring the status bar is only available on Lollipop and later
            if (Build.VERSION.SdkInt >= BuildVersionCodes.Lollipop)
            {
                try
                {
                    ((Activity)Forms.Context).Window.SetStatusBarColor(Android.Graphics.Color.ParseColor(_backgroundHexColor));
                }
                catch { }
            }
        }
    }
}