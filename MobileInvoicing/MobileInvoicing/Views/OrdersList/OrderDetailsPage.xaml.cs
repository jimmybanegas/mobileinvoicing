﻿using System.Linq;
using MobileInvoicing.Data;
using MobileInvoicing.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MobileInvoicing.Views.OrdersList
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class OrderDetailsPage : ContentPage
	{
		public OrderDetailsPage(Order order)
		{
			InitializeComponent ();

		    GridDetails.BindingContext = order.Customer;

		    lbSubTotal.Text = $"SubTotal: {order.Subtotal:C}";
		    lbTax.Text = $"Impuesto: {order.Tax:C}";
		    lbTotal.Text = $"Total: {order.Total:C}";

		    using (var data = new DataAccess())
		    {
		        order.Products = data.GetOrderDetails().Where(c => c.OrderId == order.Id).ToList();
            }

            listItemsView.ItemsSource = order.GetProducts();
        }
	}
}