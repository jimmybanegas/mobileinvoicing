﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using MobileInvoicing.Data;
using MobileInvoicing.Helpers;
using MobileInvoicing.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MobileInvoicing.Views.OrdersList
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class OrdersListPage : ContentPage
	{
	    private ObservableCollection<Order> _ordersList;
        public OrdersListPage()
		{
			InitializeComponent ();

		    var sendButton = new ToolbarItem
		    {
		        Text = "ENVIAR",
		        //Icon = "action_content_save.png",
		        Order = ToolbarItemOrder.Primary
		    };

		    sendButton.Clicked += async (sender, args) =>
		    {
		        if (NetworkCheck.IsInternet())
		        {
		            var confirm = await DisplayAlert("TAMS",
		                "Seguro desea enviar datos a TAMS?",
		                "Si", "No");

		            if (!confirm) return;

		            activityIndicator.IsRunning = true;
		            activityIndicator.IsVisible = true;

		            var response = await TamsService.InsertOrders(_ordersList);

		            activityIndicator.IsRunning = false;
		            activityIndicator.IsVisible = false;

		            if (!response)
		            {
                        await DisplayAlert("TAMS", "Ha ocurrido un error", "Ok");
		                return;
		            }

		            using (var data = new DataAccess())
		            {
		                foreach (var order in _ordersList)
		                {
		                    order.IsSent = true;
		                    data.UpdateOrder(order);
		                }

		                _ordersList = new ObservableCollection<Order>(data.GetOrders().Where(x => x.IsSent == false));
		                lvInvoices.ItemsSource = _ordersList;
		            }

		            await DisplayAlert("TAMS", "Pedidos enviados correctamente", "Ok");
                }
		        else
		        {
		            await DisplayAlert("TAMS", "No hay una conexion de red disponible", "Aceptar");
		        }
		    };

		    ToolbarItems.Add(sendButton);
        }

	    protected override void OnAppearing()
	    {
	        base.OnAppearing();

	        using (var data = new DataAccess())
	        {
	            _ordersList = new ObservableCollection<Order>(data.GetOrders().Where(x=>x.IsSent==false));

	            foreach (var order in _ordersList)
	            {
	                if (order.IsVoid)
	                {
	                    order.Customer.Name = "*****ANULADo*****";
	                }
	            }

	            lvInvoices.ItemsSource = _ordersList;
	        }
        }

	    private void SearchBar_OnTextChanged(object sender, TextChangedEventArgs e)
	    {
	        var filter = e.NewTextValue;
	        lvInvoices.BeginRefresh();
	        lvInvoices.ItemsSource = string.IsNullOrWhiteSpace(filter) ? _ordersList : _ordersList.Where(x => x.Id.ToString().ToLower().Contains(filter.ToLower()));
	        lvInvoices.EndRefresh();
        }

	    private async void lvInvoices_ItemSelected(object sender, SelectedItemChangedEventArgs e)
	    {
	        var itemSelectedData = e.SelectedItem as Order;
	        await Navigation.PushAsync(new OrderDetailsPage(itemSelectedData));
        }
	 
        private async void OnPrint(object sender, EventArgs e)
	    {
	        try
	        {
                var mi = (MenuItem)sender;

                var order = mi.CommandParameter as Order;

                await Utilities.PrintOrder(order);
	        }
	        catch (Exception exception)
	        {
                await DisplayAlert("Asegurese que la impresora está conectada", exception.Message, "Ok");

	        }

        }

	    private async void OnDelete(object sender, EventArgs e)
	    {
	        var confirm = await DisplayAlert("TAMS",
	            "Seguro desea anular pedido?",
	            "Si", "No");

	        if (!confirm) return;

	        activityIndicator.IsRunning = true;
	        activityIndicator.IsVisible = true;

            var mi = ((MenuItem)sender);

	        var order = mi.CommandParameter as Order;
	        if (order != null) order.IsVoid = true;

	        using (var data = new DataAccess())
	        {
	            data.UpdateOrder(order);   
	        }
            
	        using (var data = new DataAccess())
	        {
	            _ordersList = new ObservableCollection<Order>(data.GetOrders().Where(x => x.IsSent == false));

	            foreach (var invoiceItem in _ordersList)
	            {
	                if (invoiceItem.IsVoid)
	                {
	                    invoiceItem.Customer.Name = "*****ANULADO*****";
	                }
	            }

                lvInvoices.ItemsSource = _ordersList;
	        }

	        activityIndicator.IsRunning = false;
	        activityIndicator.IsVisible = false;
        }
	}
}