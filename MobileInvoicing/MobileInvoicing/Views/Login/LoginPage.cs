﻿using System;
using MobileInvoicing.Data;
using Xamarin.Forms;

namespace MobileInvoicing.Views.Login
{
    public class LoginPage : ContentPage
    {
        Entry username, password;
        public LoginPage(ILoginManager ilm)
        {
            var button = new Button { Text = "Ingresar" };

            button.Clicked += (sender, e) => {
                if (!(Helpers.Settings.Username.Equals(username.Text) && Helpers.Settings.Password.Equals(password.Text)))
                {
                    DisplayAlert("Error", "Usuario y contraseña incorrectos", "Re-intentar");
                }
                else
                {
                    // REMEMBER LOGIN STATUS!
                    Application.Current.Properties["IsLoggedIn"] = true;
                    ilm.ShowMainPage();
                }
            };

            username = new Entry { Text = "" };
            password = new Entry { Text = "", IsPassword = true};
            Content = new StackLayout
            {
                Padding = new Thickness(10, 40, 10, 10),
                Children = {
                    new Label { Text = "Iniciar Sesión", FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)) },
                    new Label { Text = "Usuario" },
                    username,
                    new Label { Text = "Contraseña" },
                    password,
                    button
                }
            };
        }
    }
}
