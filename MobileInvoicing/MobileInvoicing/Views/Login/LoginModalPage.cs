﻿using MobileInvoicing.Data;
using Xamarin.Forms;

namespace MobileInvoicing.Views.Login
{
    public class LoginModalPage : CarouselPage
    {
        ContentPage login;
        public LoginModalPage(ILoginManager ilm)
        {
            login = new LoginPage(ilm);
           
            Children.Add(login);

            MessagingCenter.Subscribe<ContentPage>(this, "Login", (sender) => {
                SelectedItem = login;
            });
        }
    }
}
