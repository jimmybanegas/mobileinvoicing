﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MobileInvoicing.Data;
using MobileInvoicing.Helpers;
using MobileInvoicing.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MobileInvoicing.Views.Sales
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class InvoicesListPage : ContentPage
	{
	    private ObservableCollection<Invoice> _invoicesList;
        public InvoicesListPage ()
		{
			InitializeComponent ();

		    var sendButton = new ToolbarItem
		    {
		        Text = "ENVIAR",
		        //Icon = "action_content_save.png",
		        Order = ToolbarItemOrder.Primary
		    };

		    sendButton.Clicked += async (sender, args) =>
		    {
		        if (NetworkCheck.IsInternet())
		        {
		            var confirm = await DisplayAlert("TAMS",
		                "Seguro desea enviar datos a TAMS?",
		                "Si", "No");

		            if (!confirm) return;

		            activityIndicator.IsRunning = true;
		            activityIndicator.IsVisible = true;

		            var response = await TamsService.InsertInvoices(_invoicesList);

		            activityIndicator.IsRunning = false;
		            activityIndicator.IsVisible = false;

		            if (!response)
		            {
                        await DisplayAlert("TAMS", "Ha ocurrido un error", "Ok");
		                return;
		            }

		            using (var data = new DataAccess())
		            {
		                foreach (var invoice in _invoicesList)
		                {
		                    invoice.IsSent = true;
		                    data.UpdateInvoice(invoice);
		                }

		                _invoicesList = new ObservableCollection<Invoice>(data.GetInvoices().Where(x => x.IsSent == false));
		                lvInvoices.ItemsSource = _invoicesList;
		            }

		            await DisplayAlert("TAMS", "Facturas enviadas correctamente", "Ok");
                }
		        else
		        {
		            await DisplayAlert("TAMS", "No hay una conexion de red disponible", "Aceptar");
		        }
		    };

		    ToolbarItems.Add(sendButton);
        }

	    protected override void OnAppearing()
	    {
	        base.OnAppearing();

	        using (var data = new DataAccess())
	        {
	            _invoicesList = new ObservableCollection<Invoice>(data.GetInvoices().Where(x=>x.IsSent==false));

	            foreach (var invoice in _invoicesList)
	            {
	                if (invoice.IsVoid)
	                {
	                    invoice.Customer.Name = "*****ANULADA*****";
	                }
	            }

	            lvInvoices.ItemsSource = _invoicesList;
	        }
        }

	    private void SearchBar_OnTextChanged(object sender, TextChangedEventArgs e)
	    {
	        var filter = e.NewTextValue;
	        lvInvoices.BeginRefresh();
	        lvInvoices.ItemsSource = string.IsNullOrWhiteSpace(filter) ? _invoicesList : _invoicesList.Where(x => x.InvoiceNumber.ToLower().Contains(filter.ToLower()));
	        lvInvoices.EndRefresh();
        }

	    private async void lvInvoices_ItemSelected(object sender, SelectedItemChangedEventArgs e)
	    {
	        var itemSelectedData = e.SelectedItem as Invoice;
	        await Navigation.PushAsync(new InvoiceDetailsPage(itemSelectedData));
        }
	 
        private async void OnPrint(object sender, EventArgs e)
	    {
	        try
	        {
                var mi = (MenuItem)sender;

                var invoice = mi.CommandParameter as Invoice;

                await Utilities.PrintInvoice(invoice);
	        }
	        catch (Exception exception)
	        {
                await DisplayAlert("Asegurese que la impresora está conectada", exception.Message, "Ok");

	        }

        }

	    private async void OnDelete(object sender, EventArgs e)
	    {
	        var confirm = await DisplayAlert("TAMS",
	            "Seguro desea anular factura?",
	            "Si", "No");

	        if (!confirm) return;

	        activityIndicator.IsRunning = true;
	        activityIndicator.IsVisible = true;

            var mi = ((MenuItem)sender);

	        var invoice = mi.CommandParameter as Invoice;
	        if (invoice != null) invoice.IsVoid = true;

	        using (var data = new DataAccess())
	        {
	            data.UpdateInvoice(invoice);   
	        }
            
	        using (var data = new DataAccess())
	        {
	            _invoicesList = new ObservableCollection<Invoice>(data.GetInvoices().Where(x => x.IsSent == false));

	            foreach (var invoiceItem in _invoicesList)
	            {
	                if (invoiceItem.IsVoid)
	                {
	                    invoiceItem.Customer.Name = "*****ANULADA*****";
	                }
	            }

                lvInvoices.ItemsSource = _invoicesList;
	        }

	        activityIndicator.IsRunning = false;
	        activityIndicator.IsVisible = false;
        }
	}
}