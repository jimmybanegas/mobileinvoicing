﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using MobileInvoicing.Data;
using MobileInvoicing.Helpers;
using MobileInvoicing.Models;
using Xamarin.Forms;

using Xamarin.Forms.Xaml;
using ZXing.Net.Mobile.Forms;

namespace MobileInvoicing.Views.Products
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ProductsListPage : ContentPage
	{
	    private ObservableCollection<Product> _productsList;
		public ProductsListPage ()
		{
			InitializeComponent ();

            // Create and initialize ToolbarItem.
            var syncButton = new ToolbarItem
		    {
		        Text = "Sincronizar",
		        Icon = "sync.png",
		        Order = ToolbarItemOrder.Primary
		    };

		    syncButton.Clicked += async (sender, args) =>
            {
                try
                {
                    if (NetworkCheck.IsInternet())
                    {
                        activityIndicator.IsRunning = true;
                        activityIndicator.IsVisible = true;

                        var products = await TamsService.GetProductsAsync();

                        var enumerable = products as IList<Product> ?? products.ToList();
                        if (!enumerable.Any())
                        {
                            activityIndicator.IsRunning = false;
                            activityIndicator.IsVisible = false;
                            return;
                        }

                        using (var data = new DataAccess())
                        {
                            var delete = data.DeletAllProducts();
                            //if (delete == 0) return;
                            var response = data.InsertProducts((List<Product>)products);

                            if (response == 0) return;
                            _productsList = new ObservableCollection<Product>(enumerable);
                            lvProducts.ItemsSource = _productsList;
                        }

                        activityIndicator.IsRunning = false;
                        activityIndicator.IsVisible = false;

                        await DisplayAlert("TAMS", "Lista de productos actualizada", "Aceptar");
                    }
                    else
                    {
                        await DisplayAlert("TAMS", "No hay una conexion de red disponible", "Aceptar");
                    }
                }
                catch (Exception e)
                {
                    activityIndicator.IsRunning = false;
                    activityIndicator.IsVisible = false;
                    await DisplayAlert("Error", "Ha ocurrido un error "+e.Message, "Aceptar");
                }
            };

            ToolbarItems.Add(syncButton);

		    lvProducts.ItemSelected += lvProducts_ItemSelected;
		}

	    protected override async void OnAppearing()
	    {
	        base.OnAppearing();

	        // Access data through DataAccess
	        using (var data = new DataAccess())
	        {
                _productsList = new ObservableCollection<Product>(data.GetProducts());
	            lvProducts.ItemsSource = _productsList;
            }
        }

	    private void SearchBar_OnTextChanged(object sender, TextChangedEventArgs e)
	    {
	        var filter = e.NewTextValue;
	        lvProducts.BeginRefresh();
	        lvProducts.ItemsSource = string.IsNullOrWhiteSpace(filter) ? _productsList : _productsList.Where(x => x.Name.ToLower().Contains(filter.ToLower()));
	        lvProducts.EndRefresh();
        }

	    private void lvProducts_ItemSelected(object sender, SelectedItemChangedEventArgs e)
	    {
	       // Navigation.PushAsync(new ProductsDetailPage((Models.Produto)e.SelectedItem));
	    }

	    private void ProductSearch_OnSearchButtonPressed(object sender, EventArgs e)
	    {
            var scanPage = new ZXingScannerPage();
            // Navigate to our scanner page
            //await Navigation.PushAsync(scanPage);

            //scanPage.OnScanResult += (result) =>
            //{
            //    // Stop scanning
            //    scanPage.IsScanning = false;

            //    // Pop the page and show the result
            //    Device.BeginInvokeOnMainThread(async () =>
            //    {
            //        await Navigation.PopAsync();

            //        var filter = result.Text.Trim();
            //        lvProducts.BeginRefresh();
            //        lvProducts.ItemsSource = string.IsNullOrWhiteSpace(filter) ? _productsList : _productsList.Where(x => x.Barcode.Equals(filter));
            //        lvProducts.EndRefresh();
            //    });
            //};
        }
	}
}