﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using MobileInvoicing.Data;
using MobileInvoicing.Helpers;
using MobileInvoicing.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MobileInvoicing.Views.Vendors
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class VendorsListPage : ContentPage
	{
	    private ObservableCollection<Vendor> _vendorsList;
        public VendorsListPage ()
        {
            InitializeComponent ();

            // Create and initialize ToolbarItem.
            var syncButton = new ToolbarItem
		    {
		        Text = "Sincronizar",
		        Icon = "sync.png",
		        Order = ToolbarItemOrder.Primary
		    };

		    syncButton.Clicked += async (sender, args) =>
		    {
		        try
		        {
		            if (NetworkCheck.IsInternet())
		            {
		                activityIndicator.IsRunning = true;
		                activityIndicator.IsVisible = true;

                        var vendors = await TamsService.GetVendorsAsync();

		                var enumerable = vendors as IList<Vendor> ?? vendors.ToList();
		                if (!enumerable.Any())
		                {
		                    activityIndicator.IsRunning = false;
		                    activityIndicator.IsVisible = false;
		                    return;
                        }
		                using (var data = new DataAccess())
		                {
                            var delete = data.DeletAllVendors();
		                    //if (delete == 0) return;
		                    var response = data.InsertVendors((List<Vendor>)vendors);

		                    if (response == 0) return;
		                    _vendorsList = new ObservableCollection<Vendor>(enumerable);
		                    lvVendors.ItemsSource = _vendorsList;
		                }

		                activityIndicator.IsRunning = false;
		                activityIndicator.IsVisible = false;

		                await DisplayAlert("TAMS", "Lista de vendedores actualizada", "Aceptar");
                    }
                    else
		            {
		                await DisplayAlert("TAMS", "No hay una conexion de red disponible", "Aceptar");
                    }
		        }
		        catch (Exception e)
		        {
		            activityIndicator.IsRunning = false;
		            activityIndicator.IsVisible = false;
                    await DisplayAlert("Error", "Ha ocurrido un error " + e.Message, "Aceptar");
		        }
            };

            ToolbarItems.Add(syncButton);

        }

	    protected override void OnAppearing()
	    {
	        base.OnAppearing();
	        
	        // Access data through DataAccess
	        using (var data = new DataAccess())
	        {
	            _vendorsList = new ObservableCollection<Vendor>(data.GetVendors()); 
	            lvVendors.ItemsSource = _vendorsList;
            }
	    }

        private void SearchBar_OnTextChanged(object sender, TextChangedEventArgs e)
	    {
	        var filter = e.NewTextValue;
	        lvVendors.BeginRefresh();
	        lvVendors.ItemsSource = string.IsNullOrWhiteSpace(filter) ? _vendorsList : _vendorsList.Where(x => x.Name.ToLower().Contains(filter.ToLower()));
	        lvVendors.EndRefresh();
        }
    }
}