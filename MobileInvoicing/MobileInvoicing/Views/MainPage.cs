﻿using System;
using MobileInvoicing.Views.Menu;
using Xamarin.Forms;

namespace MobileInvoicing.Views
{
    public class MainPage : MasterDetailPage
    {
        private MenuPage _menuPage;

        public MainPage()
        {
            _menuPage = new MenuPage();

            _menuPage.Menu.ItemSelected += (sender, e) => NavigateTo(e.SelectedItem as Menu.MenuItem);

            Master = _menuPage;
            Detail = new NavigationPage(new HomePage());
        }

        async void NavigateTo(Menu.MenuItem menu)
        {
            if (menu == null)
                return;

            Page displayPage = (Page)Activator.CreateInstance(menu.TargetType);

            try
            {
                Detail = new NavigationPage(displayPage);
            }
            catch (Exception ex)
            {
                await App.Current.MainPage.DisplayAlert("Error", ex.Message, "OK");
            }

            _menuPage.Menu.SelectedItem = null;
            IsPresented = false;
        }
    }
}
