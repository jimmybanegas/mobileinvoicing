﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using MobileInvoicing.Data;
using MobileInvoicing.Helpers;
using MobileInvoicing.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MobileInvoicing.Views.Customers
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CustomersListPage : ContentPage
    {

        private ObservableCollection<Customer> _customersList;
        public CustomersListPage()
        {
            InitializeComponent();

            // Create and initialize ToolbarItem.
            var syncButton = new ToolbarItem
            {
                Text = "Sincronizar",
                Icon = "sync.png",
                Order = ToolbarItemOrder.Primary
            };

            syncButton.Clicked += async (sender, args) =>
            {
                try
                {
                    if (NetworkCheck.IsInternet())
                    {
                        activityIndicator.IsRunning = true;
                        activityIndicator.IsVisible = true;

                        var customers = await TamsService.GetCustomersAsync();

                        var enumerable = customers as IList<Customer> ?? customers.ToList();
                        if (!enumerable.Any())
                        {
                            activityIndicator.IsRunning = false;
                            activityIndicator.IsVisible = false;
                            return;
                        }

                        using (var data = new DataAccess())
                        {
                            var delete = data.DeletAllCustomers();
                            //if (delete == 0) return;
                            var response = data.InsertCustomers((List<Customer>)customers);

                            if (response == 0) return;
                            _customersList = new ObservableCollection<Customer>(enumerable);
                            lvCustomers.ItemsSource = _customersList.OrderBy( x => x.Priority);
                        }

                        activityIndicator.IsRunning = false;
                        activityIndicator.IsVisible = false;

                        await DisplayAlert("TAMS", "Lista de clientes actualizada", "Aceptar");
                    }
                    else
                    {
                        await DisplayAlert("TAMS", "No hay una conexion de red disponible", "Aceptar");
                    }
                }
                catch (Exception e)
                {
                    activityIndicator.IsRunning = false;
                    activityIndicator.IsVisible = false;
                    await DisplayAlert("Error", "Ha ocurrido un error " + e.Message, "Aceptar");
                }
            };

            ToolbarItems.Add(syncButton);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            // Access data through DataAccess
            using (var data = new DataAccess())
            {
                _customersList = new ObservableCollection<Customer>(data.GetCustomers());
                lvCustomers.ItemsSource = _customersList.OrderBy( x=> x.Priority);
            }
        }

        private void SearchBar_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            var filter = e.NewTextValue;
            lvCustomers.BeginRefresh();
            lvCustomers.ItemsSource = string.IsNullOrWhiteSpace(filter) ? _customersList : _customersList.Where(x => x.Name.ToLower().Contains(filter.ToLower()));
            lvCustomers.EndRefresh();
        }

    }
}