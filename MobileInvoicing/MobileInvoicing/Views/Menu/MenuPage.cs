﻿using Xamarin.Forms;

namespace MobileInvoicing.Views.Menu
{
	public class MenuPage : ContentPage
	{   
        public ListView Menu { get; set; }

		public MenuPage ()
		{
		    Title = "Menu";
		    BackgroundColor = Color.DodgerBlue;

		    Menu = new MenuListView();

		    var menuLabel = new ContentView
		    {
		        Padding = new Thickness(10, 26, 0, 5),
		        Content = new Label
		        {
		            TextColor = Color.White,
		            Text = "MENU"
		        }
		    };

		    var logoutButton = new Button { Text = "Salir" };
		    logoutButton.Clicked += (sender, e) => {
		        App.Current.Logout();
		    };

            var layout = new StackLayout
            {
                Spacing = 5,
                Padding = 10,
                VerticalOptions = LayoutOptions.FillAndExpand
            };
            
		    layout.Children.Add(menuLabel);
		    layout.Children.Add(Menu);
		    layout.Children.Add(logoutButton);

            Content = layout;
		}
	}
}