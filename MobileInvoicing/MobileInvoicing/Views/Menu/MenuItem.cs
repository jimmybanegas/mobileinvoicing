﻿using System;

namespace MobileInvoicing.Views.Menu
{
    public class MenuItem
    {
        public string Title { get; set; }
        public string IconSource { get; set; }
        public Type TargetType { get; set; }
    }
}
