﻿using System.Collections.Generic;
using MobileInvoicing.Views.Customers;
using MobileInvoicing.Views.Orders;
using MobileInvoicing.Views.OrdersList;
using MobileInvoicing.Views.Products;
using MobileInvoicing.Views.ReprintSales;
using MobileInvoicing.Views.Settings;
using MobileInvoicing.Views.Vendors;
using InvoicesListPage = MobileInvoicing.Views.Sales.InvoicesListPage;
using SelectCustomerPage = MobileInvoicing.Views.Invoicing.SelectCustomerPage;

namespace MobileInvoicing.Views.Menu
{
    public class MenuListData : List<MenuItem>
    {
        public MenuListData()
        {
            Add(new MenuItem
            {
                Title = "Inicio",
                IconSource = "home.png",
                TargetType = typeof(HomePage)
            });

            Add(new MenuItem
            {
                Title = "Facturar",
                IconSource = "invoice.png",
                TargetType = typeof(SelectCustomerPage)
            });

            Add(new MenuItem
            {
                Title = "Orden",
                IconSource = "order.png",
                TargetType = typeof(OrderSelectCustomerPage)
            });

            Add(new MenuItem
            {
                Title = "Ventas",
                IconSource = "sales.png",
                TargetType = typeof(InvoicesListPage)
            });

            Add(new MenuItem
            {
                Title = "Pedidos",
                IconSource = "pedidos.png",
                TargetType = typeof(OrdersListPage)
            });

            Add(new MenuItem
            {
                Title = "Reimprimir",
                IconSource = "print.png",
                TargetType = typeof(ReprintInvoicesListPage)
            });

            Add(new MenuItem
            {
                Title = "Vendedores",
                IconSource = "vendors.png",
                TargetType = typeof(VendorsListPage)
            });

            Add(new MenuItem
            {
                Title = "Productos",
                IconSource = "products.png",
                TargetType = typeof(ProductsListPage)
            });

            Add(new MenuItem
            {
                Title = "Clientes",
                IconSource = "customers.png",
                TargetType = typeof(CustomersListPage),
            });

            Add(new MenuItem
            {
                Title = "Configuraciones",
                IconSource = "settings.png",
                TargetType = typeof(ConfigPage)
            });

        }
    }
}
