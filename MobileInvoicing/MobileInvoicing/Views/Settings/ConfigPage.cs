﻿using System;
using MobileInvoicing.Helpers;
using Xamarin.Forms;

namespace MobileInvoicing.Views.Settings
{
    public class ConfigPage : ContentPage
    {
        public ConfigPage()
        {
            Title = "Configuraciones";
            BindingContext = new SettingsViewModel();
       
            Entry vendor = new Entry
            {
                Placeholder = "Código de Vendedor",
                Keyboard = Keyboard.Numeric
            };

            Entry place = new Entry
            {
                Placeholder = "Establecimiento",
                Keyboard = Keyboard.Numeric
            };

            Entry point = new Entry
            {
                Placeholder = "Punto de Emision",
                Keyboard = Keyboard.Numeric
            };

            Entry begin = new Entry
            {
                Placeholder = "Factura Inicio",
                Keyboard = Keyboard.Numeric
            };

            Entry end = new Entry
            {
                Placeholder = "Factura Final",
                Keyboard = Keyboard.Numeric
            };

            Entry currentInvoice = new Entry
            {
                Placeholder = "Correlativo Actual",
                Keyboard = Keyboard.Numeric
            };

            Entry cai = new Entry
            {
                Placeholder = "Código CAI"
            };

            Entry companyName = new Entry
            {
                Placeholder = "Nombre"
            };

            Entry companyAddress = new Entry
            {
                Placeholder = "Direccion"
            };

            Entry companyRtn = new Entry
            {
                Placeholder = "RTN"
            };

            Entry companyEmail = new Entry
            {
                Placeholder = "Correo"
            };

            Entry printerName = new Entry
            {
                Placeholder = "Nombre de impresora"
            };

            Entry printInvoice = new Entry
            {
                Placeholder = "Imprimir Factura"
            };

            Entry companyPhone = new Entry
            {
                Placeholder = "Telefono"
            };

            DatePicker fechaLimite = new DatePicker
            {
                Format = "D"
            };

            Entry usuario = new Entry
            {
                Placeholder = "Usuario"
            };

            Entry password = new Entry
            {
                Placeholder = "Contraseña",
                IsPassword = true
            };

            Entry servidor = new Entry
            {
                Placeholder = "IP Servidor"
            };

            Entry baseDatos = new Entry
            {
                Placeholder = "Base de datos"
            };

            //Set bindings

            //bodega.SetBinding(Entry.TextProperty, new Binding("Bodega", converter: new DecimalConverter()));
            vendor.SetBinding(Entry.TextProperty, new Binding("Vendor", converter: new DecimalConverter()));
            place.SetBinding(Entry.TextProperty, new Binding("Place", converter: new DecimalConverter()));
            point.SetBinding(Entry.TextProperty, new Binding("Point", converter: new DecimalConverter()));
            begin.SetBinding(Entry.TextProperty, new Binding("Begin", converter: new DecimalConverter()));
            end.SetBinding(Entry.TextProperty, new Binding("End", converter: new DecimalConverter()));
            currentInvoice.SetBinding(Entry.TextProperty, new Binding("CurrentInvoice", converter: new DecimalConverter()));
            cai.SetBinding(Entry.TextProperty, new Binding("Cai", converter: new DecimalConverter()));
            companyName.SetBinding(Entry.TextProperty,"CompanyName");
            companyAddress.SetBinding(Entry.TextProperty, "CompanyAddress");
            companyRtn.SetBinding(Entry.TextProperty, "CompanyRtn");
            companyEmail.SetBinding(Entry.TextProperty, "CompanyEmail");
            companyPhone.SetBinding(Entry.TextProperty, "CompanyPhone");
            printerName.SetBinding(Entry.TextProperty, "PrinterName");
            printInvoice.SetBinding(Entry.TextProperty, "PrintInvoice");

            usuario.SetBinding(Entry.TextProperty, "Username");
            password.SetBinding(Entry.TextProperty, "Password");
            servidor.SetBinding(Entry.TextProperty, "Servidor");
            baseDatos.SetBinding(Entry.TextProperty, "BaseDatos");
            fechaLimite.SetBinding(DatePicker.DateProperty, new Binding("FechaLimite", BindingMode.TwoWay));

            var stackLayout = new StackLayout
            {
                Children = {
                    new Label { Text = "Código de Vendedor:", FontAttributes = FontAttributes.Bold },
                    vendor,
                    new Label { Text = "Establecimiento:", FontAttributes = FontAttributes.Bold },
                    place,
                    new Label { Text = "Punto de ventas:", FontAttributes = FontAttributes.Bold },
                    point,
                    new Label { Text = "Factura de Inicio:", FontAttributes = FontAttributes.Bold },
                    begin,
                    new Label { Text = "Factura Final:", FontAttributes = FontAttributes.Bold },
                    end,
                    new Label { Text = "Correlativo actual:", FontAttributes = FontAttributes.Bold },
                    currentInvoice,
                    new Label { Text = "Código CAI:", FontAttributes = FontAttributes.Bold },
                    cai,
                    new Label { Text = "Fecha Límite:", FontAttributes = FontAttributes.Bold },
                    fechaLimite,
                    new Label { Text = "Usuario:", FontAttributes = FontAttributes.Bold },
                    usuario,
                    new Label { Text = "Contraseña:", FontAttributes = FontAttributes.Bold },
                    password,
                    new Label { Text = "DATOS DE LA COMPAÑÍA", FontAttributes = FontAttributes.Bold, VerticalTextAlignment = TextAlignment.Center},
                    //new BoxView {WidthRequest = 1, HeightRequest = 1, BackgroundColor = Color.Black},
                    new Label { Text = "Nombre:", FontAttributes = FontAttributes.Bold },
                    companyName,
                    new Label { Text = "Direccion:", FontAttributes = FontAttributes.Bold },
                    companyAddress,
                    new Label { Text = "Telefono:", FontAttributes = FontAttributes.Bold },
                    companyPhone,
                    new Label { Text = "Correo:", FontAttributes = FontAttributes.Bold },
                    companyEmail,
                    new Label { Text = "RTN:", FontAttributes = FontAttributes.Bold },
                    companyRtn,
                    new Label { Text = "Impresora:", FontAttributes = FontAttributes.Bold },
                    printerName,
                    new Label { Text = "Imprimir Factura:", FontAttributes = FontAttributes.Bold },
                    printInvoice,
                    new Label { Text = "IP Servidor:", FontAttributes = FontAttributes.Bold },
                    servidor,
                    new Label { Text = "Base de datos:", FontAttributes = FontAttributes.Bold },
                    baseDatos,
                    //new Label { Text = "Bodega:" },
                    //bodega,
                    new Label { Text = "" },
                    // limpiar
                },
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Padding = 10
            };

            var scrollView = new ScrollView()
            {
                Content = stackLayout
            };

            this.Content = scrollView;

        }
    }

    internal class DecimalConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is decimal)
                return value.ToString();
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            decimal dec;
            if (decimal.TryParse(value as string, out dec))
                return dec;
            return value;
        }
    }
}