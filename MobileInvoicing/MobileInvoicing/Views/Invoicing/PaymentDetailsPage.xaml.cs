﻿using System;
using System.Collections.Generic;
using System.Linq;
using MobileInvoicing.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using static System.String;

namespace MobileInvoicing.Views.Invoicing
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PaymentDetailsPage : ContentPage
    {
        private Invoice _invoice;
        public List<InvoicePaymentDetail> PaymentDetails ;

        public PaymentDetailsPage(Invoice invoice)
        {
            InitializeComponent();

            this._invoice = invoice;

            lbTotal.Text = $"Total: {_invoice.Total:C}";

            PaymentDetails = new List<InvoicePaymentDetail>(_invoice.PaymentDetails);

            foreach (var payment in invoice.PaymentDetails)
            {
                switch (payment.PaymentMethod)
                {
                    case 1:
                        CashEntry.Text = payment.Total.ToString();
                        break;
                    case 2:
                        CheckEntry.Text = payment.Total.ToString();
                        break;
                    case 4:
                        CardEntry.Text = payment.Total.ToString();
                        break;
                    case 5:
                        DepositEntry.Text = payment.Total.ToString();
                        break;
                }
            }
            
            // Create and initialize ToolbarItem.
            var saveButton = new ToolbarItem
            {
                Text = "Guardar",
                //Icon = "action_content_save.png",
                Order = ToolbarItemOrder.Primary
            };

            saveButton.Clicked += async (sender, args) =>
            {
                var cash = IsNullOrEmpty(CashEntry.Text) ?  0 : Convert.ToDecimal(CashEntry.Text);
                var check = IsNullOrEmpty(CheckEntry.Text) ?  0 : Convert.ToDecimal(CheckEntry.Text) ;
                var card = IsNullOrEmpty(CardEntry.Text) ?  0 : Convert.ToDecimal(CardEntry.Text) ;
                var deposit = IsNullOrEmpty(DepositEntry.Text) ? 0 : Convert.ToDecimal(DepositEntry.Text) ;

                var paymentList = new List<InvoicePaymentDetail>();

                if (cash > 0)
                {
                    paymentList.Add(new InvoicePaymentDetail{ PaymentMethod = 1, Total = cash});
                }

                if (check > 0)
                {
                    paymentList.Add(new InvoicePaymentDetail { PaymentMethod = 2, Total = check });
                }

                if (card > 0)
                {
                    paymentList.Add(new InvoicePaymentDetail { PaymentMethod = 4, Total = card });
                }

                if (deposit > 0)
                {
                    paymentList.Add(new InvoicePaymentDetail { PaymentMethod = 5, Total = deposit });
                }

                if (paymentList.Sum(x=>x.Total) != _invoice.Total)
                {
                    await DisplayAlert("Error", "Las formas de pago deben ser igual a total de la factura", "Ok");
                    return;
                }

                PaymentDetails = new List<InvoicePaymentDetail>(paymentList);

                await Navigation.PopAsync();
            };

            ToolbarItems.Add(saveButton);
        }

        private void paymentCompleted(object sender, EventArgs e)
        {
            //Entry entry = sender as Entry;

            //var current = Convert.ToInt32(lbTotal.Text);

            //lbTotal.Text = String.Empty;

            //if (entry != null) lbTotal.Text = (current - Convert.ToInt32(entry.Text)).ToString();
        }
    }
}