﻿using System.Collections.ObjectModel;
using System.Linq;
using MobileInvoicing.Helpers;
using MobileInvoicing.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MobileInvoicing.Views.Invoicing
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SelectProductPage : ContentPage
    {
        private readonly ObservableCollection<Product> _productsList;
        public Product SelectedProduct = null;
        public InvoiceDetail ItemQuantity = new InvoiceDetail
        {
            Product = new Product(),
            Quantity = 1,
            Id = 0,
        };

        public SelectProductPage(ObservableCollection<Product> productsList)
        {
            InitializeComponent();

            _productsList = productsList;
            lvProducts.ItemsSource = productsList;
        }

        private void SearchBar_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            var filter = e.NewTextValue;
            lvProducts.BeginRefresh();
            lvProducts.ItemsSource = string.IsNullOrWhiteSpace(filter) ? _productsList : _productsList.Where(x => x.Name.ToLower().Contains(filter.ToLower()));
            lvProducts.EndRefresh();
        }

       
        public async void lvProducts_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var itemSelectedData = e.SelectedItem as Product;
            SelectedProduct = itemSelectedData;

            await DependencyService.Get<INativeMethods>().ShowDialog(ItemQuantity, "Cantidad");

            await Navigation.PopAsync();
        }
    }
}