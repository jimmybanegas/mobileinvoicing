﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using MobileInvoicing.Data;
using MobileInvoicing.Helpers;
using MobileInvoicing.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ZXing.Net.Mobile.Forms;

namespace MobileInvoicing.Views.Invoicing
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class InvoicePage : ContentPage
    {
        public InvoiceDetail item;
        public Invoice _invoice;
        private ObservableCollection<Product> _productsList;
        public InvoiceDetail itemQuantity = new InvoiceDetail
        {
            Product = new Product(),
            Quantity = 1,
            Id = 0,
        };

        public Product _SelectedProduct;

        public InvoicePage(Customer customer)
        {
            InitializeComponent();
            GridDetails.BindingContext = customer;
            _productsList = new ObservableCollection<Product>();

            // Initializing invoice
            _invoice = new Invoice {Customer = customer};

            this.BindingContext = _invoice;
            RefreshInvoiceData();

            // Create and initialize ToolbarItem.
            var saveButton = new ToolbarItem
            {
                Text = "Procesar",
                //Icon = "action_content_save.png",
                Order = ToolbarItemOrder.Primary
            };

            var paymentButton = new ToolbarItem
            {
                // Text = "Procesar",
                Icon = "credit_card.png",
                Order = ToolbarItemOrder.Primary
            };

            var pricesListButton = new ToolbarItem
            {
                Text = "PRECIOS",
                // Icon = "credit_card.png",
                Order = ToolbarItemOrder.Secondary
            };

            saveButton.Clicked += async (sender, args) =>
            {
                if (!(_invoice.Products.Count > 0))
                {
                    await DisplayAlert("Error", "No hay datos que procesar", "OK");
                    return;
                }

                if (_invoice.PaymentDetails.Sum(x => x.Total) != _invoice.Total)
                {
                    await DisplayAlert("Error", "Las formas de pago y el total no coinciden", "OK");
                    return;
                }

                var confirm = await DisplayAlert("TAMS",
                    "Seguro desea procesar factura?",
                    "Si", "No");

                if (!confirm) return;


                _invoice.GenerateParameters();

                using (var data = new DataAccess())
                {
                    var vendor = data.GetVendor(Helpers.Settings.Vendor);

                    if (vendor == null)
                    {
                        await DisplayAlert("Mensaje", "Codigo de vendedor no existe", "OK");
                        return;
                    }

                    _invoice.Vendor = vendor;

                    foreach (var product in _invoice.Products)
                    {
                        data.InsertInvoiceDetail(product);
                    }

                    foreach (var payment in _invoice.PaymentDetails)
                    {
                        data.InsertInvoicePaymentDetail(payment);
                    }

                    data.InsertInvoice(_invoice);

                    customer.Priority += 1000;

                    data.UpdateCustomer(customer);
                }

                Helpers.Settings.CurrentInvoice = Helpers.Settings.CurrentInvoice + 1;
                if (Helpers.Settings.PrintInvoice.ToLower().Equals("si"))
                {
                    try
                    {
                        await Utilities.PrintInvoice(_invoice);
                    }
                    catch (Exception e)
                    {
                        await DisplayAlert("Error", e.Message, "OK");
                    }
                }

                await Navigation.PopAsync();
            };

            paymentButton.Clicked += async (sender, args) =>
            {
                if (!(_invoice.Products.Count > 0))
                {
                    await DisplayAlert("Error", "La factura no tiene datos", "OK");
                    return;
                }

                var paymentDetails = new PaymentDetailsPage(_invoice);

                paymentDetails.Disappearing += OnPaymentDetailsDisappearing;

                await Navigation.PushAsync(paymentDetails);
            };

            pricesListButton.Clicked += async (sender, args) =>
            {
                var action = await DisplayActionSheet("Seleccione lista de precios:", "Cancelar", null, "Normal",
                    "Mayoreo");

                switch (action)
                {
                    case "Normal":
                        _invoice.PricesList = 1;
                        break;
                    case "Mayoreo":
                        _invoice.PricesList = 2;
                        break;
                    default:
                        _invoice.PricesList = _invoice.PricesList;
                        break;
                }

                listItemsView.ItemsSource = null;

                foreach (var detail in _invoice.Products)
                {
                    detail.Calculate(_invoice.PricesList);
                }

                listItemsView.ItemsSource = _invoice.Products;
                RefreshInvoiceData();
            };

            ToolbarItems.Add(paymentButton);
            ToolbarItems.Add(saveButton);
            ToolbarItems.Add(pricesListButton);
        }

        private void OnPaymentDetailsDisappearing(object sender, EventArgs e)
        {
            var resp = (PaymentDetailsPage) sender;

            if (resp.PaymentDetails.Count > 0)
            {
                _invoice.PaymentDetails = resp.PaymentDetails;
            }

            ((PaymentDetailsPage) sender).Disappearing -=
                OnPaymentDetailsDisappearing; //Unsubscribe from the event to allow the GC to collect the page and prevent memory leaks
        }

        private async void listViewItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                item = (InvoiceDetail) e.Item;

                if (item != null)
                {
                    listItemsView.ItemsSource = null;

                    await DependencyService.Get<INativeMethods>().ShowDialog(item, "Cantidad de " + item.Product.Name);

                    item.Calculate(_invoice.PricesList);
                    RefreshInvoiceData();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
            finally
            {
                listItemsView.ItemsSource = _invoice.Products;
            }
        }


        private void listViewItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null) return;
            ((ListView) sender).SelectedItem = null;
        }

        private async void deleteActionTapped(object sender, EventArgs e)
        {
            // item = _invoice.Products.First(x => x.Product.Id);
            //listItemsView.SelectedItem = item;
            int actionId = int.Parse(((TappedEventArgs) e).Parameter.ToString());

            var answer = await DisplayAlert("", "Desea borrar este producto?" , "OK", "CANCEL");
            listItemsView.ItemsSource = null;
            if (answer)
            {
                _invoice.Products.RemoveAll(x => x.Id == actionId);
            }

            listItemsView.ItemsSource = _invoice.Products;

            RefreshInvoiceData();
        }

        private void RefreshInvoiceData()
        {
            _invoice.Calculate();
            lbSubTotal.Text = $"SubTotal: {_invoice.Subtotal:C}";
            lbTax.Text = $"Impuesto: {_invoice.Tax:C}";
            lbTotal.Text = $"Total: {_invoice.Total:C}";
        }

        private async void entryNewItemCompleted(object sender, EventArgs e)
        {
            Entry entry = sender as Entry;

            listItemsView.ItemsSource = null;

            if (entry?.Text != null && entry.Text.Trim().Length > 0)
            {
                //var product = _products.First(x => x.Code.Trim() == entry.Text.Trim());
                Product product;
                if (_productsList.Count > 0)
                {
                    product = _productsList.FirstOrDefault(x => x.Code.Trim().Equals(entry.Text.Trim()));
                }
                else
                {
                    using (var data = new DataAccess())
                    {
                        _productsList = new ObservableCollection<Product>(data.GetProducts());
                    }

                    product = _productsList.FirstOrDefault(x => x.Code.Trim().Equals(entry.Text.Trim()));
                }

                if (product == null)
                {
                    await DisplayAlert("TAMS", "Producto no encontrado", "Aceptar");
                    listItemsView.ItemsSource = _invoice.Products;
                    return;
                }

                item = new InvoiceDetail
                {
                    Product = product,
                    Quantity = itemQuantity.Quantity,
                    Id = _invoice.Products.Count + 1
                };

                item.Calculate(_invoice.PricesList);

                if (_invoice.Products.Exists(x => x.Product.Code.Trim() == product.Code.Trim()))
                {
                    var cant = _invoice.Products.Find(x => x.Product.Code.Trim() == product.Code.Trim());
                    _invoice.Products.RemoveAll(x => x.Product.Code.Trim() == product.Code.Trim());

                    item.Quantity += cant.Quantity;
                    item.Calculate(_invoice.PricesList);
                    _invoice.Products.Insert(0, item);
                }
                else
                {
                    item.Calculate(_invoice.PricesList);
                    _invoice.Products.Insert(0, item);
                }

                listItemsView.ItemsSource = _invoice.Products;
                RefreshInvoiceData();
            }

            if (entry == null) return;
            entry.Text = "";
            entry.Unfocus();
        }

        private async void imgScanNewTapped(object sender, EventArgs e)
        {
            var scanPage = new ZXingScannerPage();
            // Navigate to our scanner page
            await Navigation.PushAsync(scanPage);
            await DependencyService.Get<INativeMethods>().ShowDialog(itemQuantity, "Cantidad");

            scanPage.OnScanResult += (result) =>
            {
                // Stop scanning
                scanPage.IsScanning = false;

                // Pop the page and show the result
                Device.BeginInvokeOnMainThread(async () =>
                {
                    await Navigation.PopAsync();
                    // await DisplayAlert("Scanned Barcode", result.Text, "OK");

                    Product product;
                    using (var data = new DataAccess())
                    {
                        product = data.GetProductByBarcode(result.Text.Trim());
                    }

                    if (product == null)
                    {
                        await DisplayAlert("TAMS", "Producto no encontrado", "Aceptar");
                        listItemsView.ItemsSource = _invoice.Products;
                        return;
                    }

                    item = new InvoiceDetail
                    {
                        Product = product,
                        Quantity = itemQuantity.Quantity,
                        Id = _invoice.Products.Count + 1
                    };
                    
                    item.Calculate(_invoice.PricesList);

                    if (_invoice.Products.Exists(x => x.Product.Code.Trim() == product.Code.Trim()))
                    {
                        var cant = _invoice.Products.Find(x => x.Product.Code.Trim() == product.Code.Trim());
                        _invoice.Products.RemoveAll(x => x.Product.Code.Trim() == product.Code.Trim());
                        // _invoice.Products.RemoveAll(cant);
                        item.Quantity += cant.Quantity;
                        item.Calculate(_invoice.PricesList);
                        _invoice.Products.Insert(0, item);
                    }
                    else
                    {
                        item.Calculate(_invoice.PricesList);
                        _invoice.Products.Insert(0, item);
                    }

                    listItemsView.ItemsSource = _invoice.Products;
                    RefreshInvoiceData();
                });
            };
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            listItemsView.ItemsSource = null;
            listItemsView.ItemsSource = _invoice.Products;

            CheckFiscalParameters();
        }

        private async void CheckFiscalParameters()
        {
            if (Helpers.Settings.Cai.Equals(string.Empty))
            {
                await DisplayAlert("Aviso", "Revise el código CAI en configuraciones", "OK");
            }

            if (Helpers.Settings.End == Helpers.Settings.CurrentInvoice)
            {
                await DisplayAlert("Aviso", "Ha llegado al límite de facturación", "OK");
            }

            if (Helpers.Settings.FechaLimite < DateTime.Now.Date)
            {
                await DisplayAlert("Aviso", "La fecha límite de facturación ha vencido", "OK");
            }
        }

        private async void imgSearchTapped(object sender, EventArgs e)
        {
            if (!(_productsList.Count > 0))
            {
                using (var data = new DataAccess())
                {
                    _productsList = new ObservableCollection<Product>(data.GetProducts());
                }
            }

            var productSearch = new SelectProductPage(_productsList);

            productSearch.Disappearing += OnProductSelectDisappearing;

            await Navigation.PushAsync(productSearch);
        }

        private void OnProductSelectDisappearing(object sender, EventArgs eventArgs)
        {
            var resp = ((SelectProductPage) sender).SelectedProduct;

            itemQuantity.Quantity = ((SelectProductPage)sender).ItemQuantity.Quantity;

            if (resp != null) entryNewItem.Text = resp.Code.Trim();

            entryNewItemCompleted(entryNewItem, null);

            ((SelectProductPage) sender).Disappearing -=
                OnProductSelectDisappearing; //Unsubscribe from the event to allow the GC to collect the page and prevent memory leaks
        }
    }
}