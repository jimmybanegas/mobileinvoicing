﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using MobileInvoicing.Data;
using MobileInvoicing.Helpers;
using MobileInvoicing.Models;
using MobileInvoicing.Views.Invoicing;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ZXing.Net.Mobile.Forms;

namespace MobileInvoicing.Views.Orders
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class OrderPage : ContentPage
    {
	    public OrderDetail item;
	    public Order _order;
	    private ObservableCollection<Product> _productsList;
        public InvoiceDetail itemQuantity = new InvoiceDetail
        {
            Product = new Product(),
            Quantity = 1,
            Id = 0,
        };

		public Product _SelectedProduct;

        public OrderPage(Customer customer)
		{
			InitializeComponent ();
		    GridDetails.BindingContext = customer;
            _productsList = new ObservableCollection<Product>();
		    
            // Initializing order
		    _order = new Order {Customer = customer};

		    this.BindingContext = _order;
            RefreshOrderData();

            // Create and initialize ToolbarItem.
            var saveButton = new ToolbarItem
		    {
		        Text = "Procesar",
		       //Icon = "action_content_save.png",
		        Order = ToolbarItemOrder.Primary
		    };


		    var pricesListButton = new ToolbarItem
		    {
		        Text = "PRECIOS",
		       // Icon = "credit_card.png",
		        Order = ToolbarItemOrder.Secondary
		    };

            saveButton.Clicked += async (sender, args) =>
		    {
		        if (!(_order.Products.Count > 0))
		        {
		            await DisplayAlert("Error", "No hay datos que procesar", "OK");
		            return;
		        }

                var confirm = await DisplayAlert("TAMS",
		            "Seguro desea procesar pedido?",
		            "Si", "No");

		        if (!confirm) return;
		      
                
		        _order.GenerateParameters();

		        using (var data = new DataAccess())
		        {

		            var vendor = data.GetVendor(Helpers.Settings.Vendor);

		            if (vendor == null)
		            {
		                await DisplayAlert("Mensaje", "Codigo de vendedor no existe", "OK");
		                return;
		            }

		            _order.Vendor = vendor;
                        
		            foreach (var product in _order.Products)
		            {
		                data.InsertOrderDetail(product);
		            }
					
                    data.InsertOrder(_order);

		            customer.Priority += 1000;

                    data.UpdateCustomer(customer);
		        }

				if (Helpers.Settings.PrintInvoice.ToLower().Equals("si"))
				{
					try
					{
						await Utilities.PrintOrder(_order);
					}
					catch (Exception e)
					{
						await DisplayAlert("Error", e.Message, "OK");
					}
				}
				await Navigation.PopAsync();
		    };

           
		    pricesListButton.Clicked += async (sender, args) =>
		    {
		        var action = await DisplayActionSheet("Seleccione lista de precios:", "Cancelar", null, "Normal", "Mayoreo");

		        switch (action)
		        {
		            case "Normal":
		                _order.PricesList = 1;
		                break;
		            case "Mayoreo":
		                _order.PricesList = 2;
		                break;
		            default:
		                _order.PricesList = _order.PricesList;
		                break;
		        }

		        listItemsView.ItemsSource = null;

		        foreach (var detail in _order.Products)  {
		            detail.Calculate(_order.PricesList);
		        }

		        listItemsView.ItemsSource = _order.Products;
                RefreshOrderData();
            };

            ToolbarItems.Add(saveButton);
		    ToolbarItems.Add(pricesListButton);
        }

        private async void listViewItemTapped(object sender, ItemTappedEventArgs e)
	    {
	        try
	        {
	            item = (OrderDetail)e.Item;

	            if (item != null)
	            {
	                listItemsView.ItemsSource = null;
	             
	                await DependencyService.Get<INativeMethods>().ShowOrderDialog(item, "Cantidad de " + item.Product.Name);
	              
                    item.Calculate(_order.PricesList);
	                RefreshOrderData();
                }
	        }
	        catch (Exception ex)
	        {
	            Debug.WriteLine(ex.Message);
	        }
	        finally
	        {
	           listItemsView.ItemsSource = _order.Products;
	        }
        }

	    private void listViewItemSelected(object sender, SelectedItemChangedEventArgs e)
	    {
	        if (e.SelectedItem == null) return;
	        ((ListView)sender).SelectedItem = null;
        }

	    private async void deleteActionTapped(object sender, EventArgs e)
	    {
            // item = _order.Products.First(x => x.Product.Id);
            //listItemsView.SelectedItem = item;
	        int actionId = int.Parse(((TappedEventArgs)e).Parameter.ToString());

            var answer = await DisplayAlert("", "Desea borrar este producto?", "OK", "CANCEL");
	        listItemsView.ItemsSource = null;
            if (answer)
	        {
	            _order.Products.RemoveAll(x => x.Id == actionId);
            }
	        listItemsView.ItemsSource = _order.Products;

            RefreshOrderData();
	    }

	    private void RefreshOrderData()
	    {
	        _order.Calculate();
	        lbSubTotal.Text = $"SubTotal: {_order.Subtotal:C}";
	        lbTax.Text = $"Impuesto: {_order.Tax:C}";
	        lbTotal.Text = $"Total: {_order.Total:C}";
	    }

	    private async void entryNewItemCompleted(object sender, EventArgs e)
	    {
	        Entry entry = sender as Entry;

            // item = new InvoiceDetail();
	        listItemsView.ItemsSource = null;

	        if (entry?.Text != null && entry.Text.Trim().Length > 0)
	        {
	            //var product = _products.First(x => x.Code.Trim() == entry.Text.Trim());
	            Product product;
	            if (_productsList.Count > 0)
	            {
	                product = _productsList.FirstOrDefault(x => x.Code.Trim().Equals(entry.Text.Trim()));
	            }   
	            else
	            {
                    using (var data = new DataAccess())
                    {
                        _productsList = new ObservableCollection<Product>(data.GetProducts());
                    }

	                product = _productsList.FirstOrDefault(x => x.Code.Trim().Equals(entry.Text.Trim()));
                }

	            if (product == null)
	            {
	                await DisplayAlert("TAMS", "Producto no encontrado", "Aceptar");
	                listItemsView.ItemsSource = _order.Products;
                    return;
	            }

                item = new OrderDetail
	            {
	                Product = product,
					Quantity = itemQuantity.Quantity,
					Id = _order.Products.Count + 1
                };

                item.Calculate(_order.PricesList);
	           
                if (_order.Products.Exists(x => x.Product.Code.Trim() == product.Code.Trim()))
	            {
	                var cant = _order.Products.Find(x => x.Product.Code.Trim() == product.Code.Trim());
                    _order.Products.RemoveAll(x => x.Product.Code.Trim() == product.Code.Trim());
	              
	                item.Quantity += cant.Quantity;
                    item.Calculate(_order.PricesList);
	                _order.Products.Insert(0, item);
	            }
	            else
	            {
	                item.Calculate(_order.PricesList);
                    _order.Products.Insert(0, item);
                }

	            listItemsView.ItemsSource = _order.Products;
	            RefreshOrderData();
            }

	        if (entry == null) return;
	        entry.Text = "";
	        entry.Unfocus();
	    }

	    private async void imgScanNewTapped(object sender, EventArgs e)
	    {
	        var scanPage = new ZXingScannerPage();
	        // Navigate to our scanner page
	        await Navigation.PushAsync(scanPage);

	        scanPage.OnScanResult += (result) =>
	        {
	            // Stop scanning
	            scanPage.IsScanning = false;

	            // Pop the page and show the result
	            Device.BeginInvokeOnMainThread(async () =>
	            {
	                await Navigation.PopAsync();
                    // await DisplayAlert("Scanned Barcode", result.Text, "OK");

	                Product product;
	                using (var data = new DataAccess())
	                {
	                    product = data.GetProductByBarcode(result.Text.Trim());
	                }

	                if (product == null)
	                {
	                    await DisplayAlert("TAMS", "Producto no encontrado", "Aceptar");
	                    listItemsView.ItemsSource = _order.Products;
	                    return;
	                }

	                item = new OrderDetail
	                {
	                    Product = product,
						Quantity = itemQuantity.Quantity,
						Id = _order.Products.Count + 1
	                };

	                item.Calculate(_order.PricesList);

	                if (_order.Products.Exists(x => x.Product.Code.Trim() == product.Code.Trim()))
	                {
	                    var cant = _order.Products.Find(x => x.Product.Code.Trim() == product.Code.Trim());
	                    _order.Products.RemoveAll(x => x.Product.Code.Trim() == product.Code.Trim());
	                    // _order.Products.RemoveAll(cant);
	                    item.Quantity += cant.Quantity;
	                    item.Calculate(_order.PricesList);
	                    _order.Products.Insert(0, item);
	                }
	                else
	                {
	                    item.Calculate(_order.PricesList);
	                    _order.Products.Insert(0, item);
	                }

	                listItemsView.ItemsSource = _order.Products;
	                RefreshOrderData();
                });
	        };
        }

	    protected override void OnAppearing()
	    {
	        base.OnAppearing();

            listItemsView.ItemsSource = null;
            listItemsView.ItemsSource = _order.Products;
        }

        private async void imgSearchTapped(object sender, EventArgs e)
	    {
	        if (!(_productsList.Count > 0))
	        {
	            using (var data = new DataAccess())
	            {
	                _productsList = new ObservableCollection<Product>(data.GetProducts());
	            }
            }

            var productSearch = new SelectProductPage(_productsList);

            productSearch.Disappearing += OnProductSelectDisappearing;

	        await Navigation.PushAsync(productSearch);
        }

        private void OnProductSelectDisappearing(object sender, EventArgs eventArgs)
        {
            var resp = ((SelectProductPage) sender).SelectedProduct;
            itemQuantity.Quantity = ((SelectProductPage)sender).ItemQuantity.Quantity;

			if (resp != null) entryNewItem.Text = resp.Code.Trim();
            entryNewItemCompleted(entryNewItem, null);
           
            ((SelectProductPage)sender).Disappearing -= OnProductSelectDisappearing; //Unsubscribe from the event to allow the GC to collect the page and prevent memory leaks

        }
    }
}