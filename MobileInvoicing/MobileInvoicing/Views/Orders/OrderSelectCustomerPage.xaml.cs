﻿using System.Collections.ObjectModel;
using System.Linq;
using MobileInvoicing.Data;
using MobileInvoicing.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MobileInvoicing.Views.Orders
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OrderSelectCustomerPage : ContentPage
    {
        private ObservableCollection<Customer> _customersList;
        public OrderSelectCustomerPage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            // Access data through DataAccess
            using (var data = new DataAccess())
            {
                _customersList = new ObservableCollection<Customer>(data.GetCustomers());
                lvCustomers.ItemsSource = _customersList.OrderBy(x => x.Priority);
            }
        }

        private void SearchBar_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            var filter = e.NewTextValue;
            lvCustomers.BeginRefresh();
            lvCustomers.ItemsSource = string.IsNullOrWhiteSpace(filter) ? _customersList : _customersList.Where(x => x.Name.ToLower().Contains(filter.ToLower()));
            lvCustomers.EndRefresh();
        }

        private async void lvCustomers_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var itemSelectedData = e.SelectedItem as Customer;
            await Navigation.PushAsync(new OrderPage(itemSelectedData));
        }
    }
}