﻿using System.Linq;
using MobileInvoicing.Data;
using MobileInvoicing.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MobileInvoicing.Views.ReprintSales
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ReprintInvoiceDetailsPage : ContentPage
	{
		public ReprintInvoiceDetailsPage(Invoice invoice)
		{
			InitializeComponent ();

		    GridDetails.BindingContext = invoice.Customer;

		    lbSubTotal.Text = $"SubTotal: {invoice.Subtotal:C}";
		    lbTax.Text = $"Impuesto: {invoice.Tax:C}";
		    lbTotal.Text = $"Total: {invoice.Total:C}";

		    using (var data = new DataAccess())
		    {
		        invoice.Products = data.GetInvoiceDetails().Where(c => c.InvoiceId == invoice.Id).ToList();
		    }

            listItemsView.ItemsSource = invoice.GetProducts();
        }
	}
}