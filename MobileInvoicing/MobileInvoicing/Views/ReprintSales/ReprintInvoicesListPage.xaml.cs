﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using MobileInvoicing.Data;
using MobileInvoicing.Helpers;
using MobileInvoicing.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MobileInvoicing.Views.ReprintSales
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ReprintInvoicesListPage : ContentPage
	{
	    private ObservableCollection<Invoice> _invoicesList;
        public ReprintInvoicesListPage()
		{
			InitializeComponent ();
        }

	    protected override void OnAppearing()
	    {
	        base.OnAppearing();

	        using (var data = new DataAccess())
	        {
	            _invoicesList = new ObservableCollection<Invoice>(data.GetSentInvoices().Where(x=>x.IsSent));

	            foreach (var invoice in _invoicesList)
	            {
	                if (invoice.IsVoid)
	                {
	                    invoice.Customer.Name = "*****ANULADA*****";
	                }
	            }

	            lvInvoices.ItemsSource = _invoicesList;
	        }
        }

	    private void SearchBar_OnTextChanged(object sender, TextChangedEventArgs e)
	    {
	        var filter = e.NewTextValue;
	        lvInvoices.BeginRefresh();
	        lvInvoices.ItemsSource = string.IsNullOrWhiteSpace(filter) ? _invoicesList : _invoicesList.Where(x => x.InvoiceNumber.ToLower().Contains(filter.ToLower()));
	        lvInvoices.EndRefresh();
        }

	    private async void lvInvoices_ItemSelected(object sender, SelectedItemChangedEventArgs e)
	    {
	        var itemSelectedData = e.SelectedItem as Invoice;
	        await Navigation.PushAsync(new ReprintInvoiceDetailsPage(itemSelectedData));
        }
	 
        private async void OnPrint(object sender, EventArgs e)
	    {
	        try
	        {
                var mi = (MenuItem)sender;

                var invoice = mi.CommandParameter as Invoice;

				await Utilities.PrintInvoice(invoice);
	        }
	        catch (Exception exception)
	        {
                await DisplayAlert("Asegurese que la impresora está conectada", exception.Message, "Ok");
	        }

        }
    }
}