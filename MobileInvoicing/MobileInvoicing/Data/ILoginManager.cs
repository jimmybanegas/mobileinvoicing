﻿namespace MobileInvoicing.Data
{
    public interface ILoginManager
    {
        void ShowMainPage();
        void Logout();
    }
}
