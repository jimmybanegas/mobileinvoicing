﻿using SQLite.Net.Interop;

namespace MobileInvoicing.Data
{
    public interface IDataBase
    {
        string DatabaseDirectory { get; }
        ISQLitePlatform Plataform { get; }
    }
}
