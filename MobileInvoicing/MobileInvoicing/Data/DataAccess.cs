﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using MobileInvoicing.Models;
using SQLite.Net;
using SQLiteNetExtensions.Extensions;
using Xamarin.Forms;

namespace MobileInvoicing.Data
{
    public class DataAccess : IDisposable
    {
        private readonly SQLiteConnection _connection;

        public DataAccess()
        {
           var config = DependencyService.Get<IDataBase>();
            _connection = new SQLiteConnection(config.Plataform, Path.Combine(config.DatabaseDirectory, "tams.db3"), false);

            _connection.CreateTable<Customer>();
            _connection.CreateTable<Vendor>();
            _connection.CreateTable<Product>();
            _connection.CreateTable<Invoice>();
            _connection.CreateTable<InvoiceDetail>();
            _connection.CreateTable<InvoicePaymentDetail>();
            _connection.CreateTable<Order>();
            _connection.CreateTable<OrderDetail>();
        }

        public void Dispose()
        {
            _connection.Dispose();
        }

        // Products
        public void InsertProduct(Product product)
        {
            _connection.Insert(product);
        }

        public void DeleteProduct(Product product)
        {
            _connection.Delete(product);
        }

        public Product GetProduct(int productId)
        {
            return _connection.Table<Product>().FirstOrDefault(c => c.Id == productId);
        }

        public Product GetProduct(string productId)
        {
            return _connection.Table<Product>().FirstOrDefault(c => c.Code.Trim() == productId);
        }

        public Product GetProductByBarcode(string barcode)
        {
            return _connection.Table<Product>().FirstOrDefault(c => c.Barcode.Trim() == barcode);
        }

        public List<Product> GetProducts()
        {
            return _connection.Table<Product>().OrderBy(c => c.Id).ToList();
        }

        public int DeletAllProducts()
        {
            return _connection.DeleteAll<Product>();
        }

        public int InsertProducts(List<Product> products)
        {
            return _connection.InsertAll(products);
        }

        
        // Customers
        public void InsertCustomer(Customer customer)
        {
            _connection.Insert(customer);
        }

        public void DeleteCustomer(Customer customer)
        {
            _connection.Delete(customer);
        }

        public Customer GetCustomer(int customerId)
        {
            return _connection.Table<Customer>().FirstOrDefault(c => c.Id == customerId);
        }

        public List<Customer> GetCustomers()
        {
            return _connection.Table<Customer>().OrderBy(c => c.Id).ToList();
        }

        public int DeletAllCustomers()
        {
            return _connection.DeleteAll<Customer>();
        }

        public int InsertCustomers(List<Customer> customers)
        {
            return _connection.InsertAll(customers);
        }

        public void UpdateCustomer(Customer customer)
        {
            _connection.Update(customer);
        }

        // Vendors
        public void InsertVendor(Vendor vendor)
        {
            _connection.Insert(vendor);
        }

        public void DeleteVendor(Vendor vendor)
        {
            _connection.Delete(vendor);
        }

        public Vendor GetVendor(int vendorCode)
        {
            return _connection.Table<Vendor>().FirstOrDefault(c => c.Code == vendorCode);
        }

        public List<Vendor> GetVendors()
        {
            return _connection.Table<Vendor>().OrderBy(c => c.Id).ToList();
        }

        public int DeletAllVendors()
        {
            return _connection.DeleteAll<Vendor>();
        }

        public int InsertVendors(List<Vendor> vendors)
        {
            return _connection.InsertAll(vendors);
        }

        // Invoices
        public void InsertInvoice(Invoice invoice)
        {
            _connection.InsertWithChildren(invoice);
        }

        public void UpdateInvoice(Invoice invoice)
        {
            _connection.UpdateWithChildren(invoice);
        }

        public void DeleteInvoice(Invoice invoice)
        {
            _connection.Delete(invoice);
        }

        public Invoice GetInvoice(int invoiceId)
        {
            return _connection.GetAllWithChildren<Invoice>().FirstOrDefault(c => c.Id == invoiceId);
        }

        public List<Invoice> GetInvoices()
        {
            return _connection.GetAllWithChildren<Invoice>().Where(x=>x.IsSent == false).OrderBy(c => c.InvoiceNumber).ToList();
        }

        public List<Invoice> GetSentInvoices()
        {
            return _connection.GetAllWithChildren<Invoice>().Where(x => x.IsSent).OrderBy(c => c.InvoiceNumber).ToList();
        }

        // Product details
        public void InsertInvoiceDetail(InvoiceDetail invoiceDetail)
        {
            _connection.InsertWithChildren(invoiceDetail);
        }

        public void UpdateInvoiceDetail(InvoiceDetail invoiceDetail)
        {
            _connection.UpdateWithChildren(invoiceDetail);
        }

        public void DeleteInvoiceDetail(InvoiceDetail invoiceDetail)
        {
            _connection.Delete(invoiceDetail);
        }

        public InvoiceDetail GetInvoiceDetail(int productDetailId)
        {
            return _connection.GetAllWithChildren<InvoiceDetail>().FirstOrDefault(c => c.Id == productDetailId);
        }

        public List<InvoiceDetail> GetInvoiceDetails()
        {
            return _connection.GetAllWithChildren<InvoiceDetail>().OrderBy(c => c.Id).ToList();
        }

        // Payment details
        public void InsertInvoicePaymentDetail(InvoicePaymentDetail invoicePayment)
        {
            _connection.InsertWithChildren(invoicePayment);
        }

        public void UpdateInvoicePaymentDetail(InvoicePaymentDetail invoicePayment)
        {
            _connection.UpdateWithChildren(invoicePayment);
        }

        public void DeleteInvoicePaymentDetail(InvoicePaymentDetail invoicePayment)
        {
            _connection.Delete(invoicePayment);
        }

        public InvoicePaymentDetail GetInvoicePaymentDetail(int paymentDetailId)
        {
            return _connection.GetAllWithChildren<InvoicePaymentDetail>().FirstOrDefault(c => c.Id == paymentDetailId);
        }

        public List<InvoicePaymentDetail> GetInvoicePaymentDetails()
        {
            return _connection.GetAllWithChildren<InvoicePaymentDetail>().OrderBy(c => c.Id).ToList();
        }

        // Orders
        public void InsertOrder(Order order)
        {
            _connection.InsertWithChildren(order);
        }

        public void UpdateOrder(Order order)
        {
            _connection.UpdateWithChildren(order);
        }

        public void DeleteOrder(Order order)
        {
            _connection.Delete(order);
        }

        public Order GetOrder(int orderId)
        {
            return _connection.GetAllWithChildren<Order>().FirstOrDefault(c => c.Id == orderId);
        }

        public List<Order> GetOrders()
        {
            return _connection.GetAllWithChildren<Order>().Where(x => x.IsSent == false).OrderBy(c => c.Id).ToList();
        }

        public List<Order> GetSentOrders()
        {
            return _connection.GetAllWithChildren<Order>().Where(x => x.IsSent).OrderBy(c => c.Id).ToList();
        }

        // Product details
        public void InsertOrderDetail(OrderDetail orderDetail)
        {
            _connection.InsertWithChildren(orderDetail);
        }

        public void UpdateOrderDetail(OrderDetail orderDetail)
        {
            _connection.UpdateWithChildren(orderDetail);
        }

        public void DeleteOrderDetail(OrderDetail orderDetail)
        {
            _connection.Delete(orderDetail);
        }

        public OrderDetail GetOrderDetail(int productDetailId)
        {
            return _connection.GetAllWithChildren<OrderDetail>().FirstOrDefault(c => c.Id == productDetailId);
        }

        public List<OrderDetail> GetOrderDetails()
        {
            return _connection.GetAllWithChildren<OrderDetail>().OrderBy(c => c.Id).ToList();
        }


    }
}
