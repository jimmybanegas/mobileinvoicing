﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using MobileInvoicing.Models;
using Xamarin.Forms;

namespace MobileInvoicing.Data
{
    public class TamsService
    {
        private static readonly ITamsService TamsHelper = DependencyService.Get<ITamsService>();

        public static Task<IEnumerable<Product>> GetProductsAsync()
        {
            return TamsHelper.GetProductsAsync();
        }

        public static Task<IEnumerable<Customer>> GetCustomersAsync()
        {
            return TamsHelper.GetCustomersAsync();
        }

        public static Task<IEnumerable<Vendor>> GetVendorsAsync()
        {
            return TamsHelper.GetVendorsAsync();
        }

        public static Task<bool> InsertInvoices(ObservableCollection<Invoice> invoices)
        {
            return TamsHelper.SendInvoices(invoices);
        }
        public static Task<bool> InsertOrders(ObservableCollection<Order> orders)
        {
            return TamsHelper.SendOrders(orders);
        }

    }
}
