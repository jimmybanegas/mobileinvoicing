﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using MobileInvoicing.Models;

namespace MobileInvoicing.Data
{
    public interface ITamsService
    {
        Task<IEnumerable<Product>> GetProductsAsync();

        Task<IEnumerable<Customer>> GetCustomersAsync();

        Task<IEnumerable<Vendor>> GetVendorsAsync();

        Task<bool> SendInvoices(ObservableCollection<Invoice> invoices);

        Task<bool> SendOrders(ObservableCollection<Order> orders);

    }
}
