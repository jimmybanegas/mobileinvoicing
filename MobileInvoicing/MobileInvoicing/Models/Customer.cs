﻿using SQLite.Net.Attributes;

namespace MobileInvoicing.Models
{
    public class Customer
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        public int Code { get; set; }

        public string Name { get; set; }

        public string RtnOrId { get; set; }

        public int Priority { get; set; }
    }
}
