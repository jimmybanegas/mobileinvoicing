﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite.Net.Attributes;

namespace MobileInvoicing.Models
{
    public class Vendor
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        public int Code { get; set; }

        public string Name { get; set; }
    }
}
