﻿using SQLite.Net.Attributes;

namespace MobileInvoicing.Models
{
    public class Product
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        public string Code { get; set; }

        public string Barcode { get; set; }

        public string Name { get; set; }

        public decimal Price { get; set; }

        public decimal Price2 { get; set; }

        public decimal TaxPercentage { get; set; }

        public string Measure { get; set; }
       
    }
}
