﻿using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;

namespace MobileInvoicing.Models
{
    public class InvoiceDetail
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        [OneToOne]
        public Product Product { get; set; }

        [ForeignKey(typeof(Product))]
        public int ProductId { get; set; }

        public decimal Price { get; set; }

        [ManyToOne]
        public Invoice Invoice { get; set; }

        [ForeignKey(typeof(Invoice))]
        public int InvoiceId { get; set; }

        public int Quantity { get; set; }

        public decimal Subtotal { get; set; }
        public decimal SubtotalTax2 { get; set; }

        public decimal SubtotalTax { get; set; }

        public decimal SubtotalNoTax { get; set; }

        public decimal Discount { get; set; }

        public decimal NoTax { get; set; }
        public decimal Tax { get; set; }
        public decimal Tax2 { get; set; }
        public decimal Total { get; set; }

        public void Calculate(int invoicePricesList)
        {
            this.Price = invoicePricesList == 2 ? Product.Price2 : Product.Price;
            this.Subtotal = Quantity * Price;
            this.SubtotalNoTax = Product.TaxPercentage == 0 ? Quantity * Price : 0;
            this.SubtotalTax = Product.TaxPercentage == 15 ? Quantity * Price : 0;
            this.SubtotalTax2 = Product.TaxPercentage == 18 ? Quantity * Price : 0;
            this.NoTax = Product.TaxPercentage == 0 ? SubtotalNoTax * (Product.TaxPercentage / 100) : 0;
            this.Tax = Product.TaxPercentage == 15 ? SubtotalTax * (Product.TaxPercentage / 100) : 0;
            this.Tax2 = Product.TaxPercentage == 18 ? SubtotalTax2 * (Product.TaxPercentage / 100) : 0;
            this.Total = Subtotal + Tax + Tax2 + NoTax;
        }
    }
}
