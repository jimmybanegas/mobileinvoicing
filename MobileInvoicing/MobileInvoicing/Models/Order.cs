﻿using System;
using System.Collections.Generic;
using System.Linq;
using MobileInvoicing.Helpers;
using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;

namespace MobileInvoicing.Models
{
    public class Order
    {
        public Order()
        {
            Products = new List<OrderDetail>();
        }

        [PrimaryKey, AutoIncrement] public int Id { get; set; }

        public DateTime Date { get; set; }

        [OneToOne] public Customer Customer { get; set; }

        [ForeignKey(typeof(Customer))] public int CustomerId { get; set; }

        public decimal Subtotal { get; set; }

        public decimal Discount { get; set; }

        public decimal Tax { get; set; }

        public decimal Tax2 { get; set; }

        public decimal NoTax { get; set; }

        public decimal SubTotalTax2 { get; set; }

        public decimal SubTotalTax { get; set; }

        public decimal SubTotalNoTax { get; set; }

        public decimal Total { get; set; }

        [OneToOne] public Vendor Vendor { get; set; }

        [ForeignKey(typeof(Vendor))] public int VendorId { get; set; }

        public int PricesList { get; set; }

        public bool IsVoid { get; set; }

        public bool IsSent { get; set; }
        public int SalesPoint { get; set; }

        [OneToMany(CascadeOperations = CascadeOperation.CascadeRead)]
        public List<OrderDetail> Products { get; set; }

        public List<OrderDetail> GetProducts()
        {
            return Products;
        }

        public void Calculate()
        {
            Subtotal = Products.Sum(x => x.Subtotal);
            SubTotalNoTax = Products.Sum(x => x.SubtotalNoTax);
            SubTotalTax = Products.Sum(x => x.SubtotalTax);
            SubTotalTax2 = Products.Sum(x => x.SubtotalTax2);
            NoTax = Products.Sum(x => x.NoTax);
            Tax = Products.Sum(x => x.Tax);
            Tax2 = Products.Sum(x => x.Tax2);
            Total = Products.Sum(x => x.Total);
        }

        public void GenerateParameters()
        {
            SalesPoint = Settings.Point;
            Date = DateTime.Now;
            IsVoid = false;
            IsSent = false;
        }
    }
}