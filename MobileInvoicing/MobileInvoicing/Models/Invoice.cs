﻿using System;
using System.Collections.Generic;
using System.Linq;
using MobileInvoicing.Helpers;
using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;

namespace MobileInvoicing.Models
{
    public class Invoice
    {
        public Invoice()
        {
            Products = new List<InvoiceDetail>();
            PaymentDetails = new List<InvoicePaymentDetail>();
        }

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        public string InvoiceNumber { get; set; }

        public DateTime Date { get; set; }

        [OneToOne]
        public Customer Customer { get; set; }

        [ForeignKey(typeof(Customer))]
        public int CustomerId { get; set; }

        public decimal Subtotal { get; set; }

        public decimal Discount { get; set; }

        public decimal Tax { get; set; }

        public decimal Tax2 { get; set; }

        public decimal NoTax { get; set; }

        public decimal SubTotalTax2 { get; set; }

        public decimal SubTotalTax { get; set; }

        public decimal SubTotalNoTax { get; set; }

        public decimal Total { get; set; }

        [OneToOne]
        public Vendor Vendor { get; set; }

        [ForeignKey(typeof(Vendor))]
        public int VendorId { get; set; }

        public string Cai { get; set; }

        public DateTime LimitDate { get; set; }

        public int Stablishment { get; set; }

        public int SalesPoint { get; set; }

        public int TypeOfDocument { get; set; }

        public int BeginDocument { get; set; }

        public int EndDocument { get; set; }

        public int PricesList { get; set; }

        public bool IsVoid { get; set; }

        public bool IsSent { get; set; }

        [OneToMany(CascadeOperations = CascadeOperation.CascadeRead)]
        public List<InvoiceDetail> Products { get; set; }

        public List<InvoiceDetail> GetProducts()
        {
            return Products;
        }

        [OneToMany(CascadeOperations = CascadeOperation.CascadeRead)]
        public List<InvoicePaymentDetail> PaymentDetails { get; set; }

        public List<InvoicePaymentDetail> GetPaymentDetails()
        {
            return PaymentDetails;
        }

        public void Calculate()
        {
            Subtotal = Products.Sum(x => x.Subtotal);
            SubTotalNoTax = Products.Sum(x => x.SubtotalNoTax);
            SubTotalTax = Products.Sum(x => x.SubtotalTax);
            SubTotalTax2 = Products.Sum(x => x.SubtotalTax2);
            NoTax = Products.Sum(x => x.NoTax);
            Tax = Products.Sum(x => x.Tax);
            Tax2 = Products.Sum(x => x.Tax2);
            Total = Products.Sum(x => x.Total);
        }

        public void GenerateParameters()
        {
            InvoiceNumber = Settings.Place.ToString().PadLeft(3, '0') + "-" + Settings.Point.ToString().PadLeft(3, '0') + "-01-" + (Settings.CurrentInvoice + 1).ToString().PadLeft(8, '0');

            Stablishment = Settings.Place;
            Date = DateTime.Now;
            Cai = Settings.Cai;
            SalesPoint = Settings.Point;
            TypeOfDocument = 1; // For invoices
            BeginDocument = Settings.Begin;
            EndDocument = Settings.End;
            LimitDate = Settings.FechaLimite;
            IsVoid = false;
            IsSent = false;
        }

    }
}
