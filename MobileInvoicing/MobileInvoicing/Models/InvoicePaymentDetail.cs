﻿using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;

namespace MobileInvoicing.Models
{
    public class InvoicePaymentDetail
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        [ManyToOne]
        public Invoice Invoice { get; set; }

        [ForeignKey(typeof(Invoice))]
        public int InvoiceId { get; set; }

        public int PaymentMethod { get; set; }

        public decimal Total { get; set; }
    }
}
