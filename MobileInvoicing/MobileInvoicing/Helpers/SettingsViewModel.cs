﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Xamarin.Forms;

namespace MobileInvoicing.Helpers
{
    public class SettingsViewModel : INotifyPropertyChanged
    {
        public string Username
        {
            get => Settings.Username;
            set
            {
                if (Settings.Username == value)
                    return;

                Settings.Username = value;
                OnPropertyChanged();
            }
        }

        public string Password
        {
            get => Settings.Password;
            set
            {
                if (Settings.Password == value)
                    return;

                Settings.Password = value;
                OnPropertyChanged();
            }
        }

      
        public string Servidor
        {
            get => Settings.Servidor;
            set
            {
                if (Settings.Servidor == value)
                    return;

                Settings.Servidor = value;
                OnPropertyChanged();
            }
        }

        public string BaseDatos
        {
            get => Settings.BaseDatos;
            set
            {
                if (Settings.BaseDatos == value)
                    return;

                Settings.BaseDatos = value;
                OnPropertyChanged();
            }
        }


        public DateTime FechaLimite
        {
            get => Settings.FechaLimite;
            set
            {
                if (Settings.FechaLimite == value)
                    return;

                Settings.FechaLimite = value;
                OnPropertyChanged();
            }
        }

        public int Vendor
        {
            get => Settings.Vendor;
            set
            {
                if (Settings.Vendor == value)
                    return;

                Settings.Vendor = value;
                OnPropertyChanged();
            }
        }

        public int Place
        {
            get => Settings.Place;
            set
            {
                if (Settings.Place == value)
                    return;

                Settings.Place = value;
                OnPropertyChanged();
            }
        }

        public int Point
        {
            get => Settings.Point;
            set
            {
                if (Settings.Point == value)
                    return;

                Settings.Point = value;
                OnPropertyChanged();
            }
        }

        public int Begin
        {
            get => Settings.Begin;
            set
            {
                if (Settings.Begin == value)
                    return;

                Settings.Begin = value;
                OnPropertyChanged();
            }
        }

        public int End
        {
            get => Settings.End;
            set
            {
                if (Settings.End == value)
                    return;

                Settings.End = value;
                OnPropertyChanged();
            }
        }

        public string Cai
        {
            get => Settings.Cai;
            set
            {
                if (Settings.Cai == value)
                    return;

                Settings.Cai = value;
                OnPropertyChanged();
            }
        }

        public string CompanyName
        {
            get => Settings.CompanyName;
            set
            {
                if (Settings.CompanyName == value)
                    return;

                Settings.CompanyName = value;
                OnPropertyChanged();
            }
        }

        public string CompanyAddress
        {
            get => Settings.CompanyAddress;
            set
            {
                if (Settings.CompanyAddress == value)
                    return;

                Settings.CompanyAddress = value;
                OnPropertyChanged();
            }
        }

        public string CompanyEmail
        {
            get => Settings.CompanyEmail;
            set
            {
                if (Settings.CompanyEmail == value)
                    return;

                Settings.CompanyEmail = value;
                OnPropertyChanged();
            }
        }

        public string CompanyRtn
        {
            get => Settings.CompanyRtn;
            set
            {
                if (Settings.CompanyRtn == value)
                    return;

                Settings.CompanyRtn = value;
                OnPropertyChanged();
            }
        }

        public string CompanyPhone
        {
            get => Settings.CompanyPhone;
            set
            {
                if (Settings.CompanyPhone == value)
                    return;

                Settings.CompanyPhone = value;
                OnPropertyChanged();
            }
        }

        public string PrinterName
        {
            get => Settings.PrinterName;
            set
            {
                if (Settings.PrinterName == value)
                    return;

                Settings.PrinterName = value;
                OnPropertyChanged();
            }
        }

        public string PrintInvoice
        {
            get => Settings.PrintInvoice;
            set
            {
                if (Settings.PrintInvoice == value)
                    return;

                Settings.PrintInvoice = value;
                OnPropertyChanged();
            }
        }

        public int CurrentInvoice
        {
            get => Settings.CurrentInvoice;
            set
            {
                if (Settings.CurrentInvoice == value)
                    return;

                Settings.CurrentInvoice = value;
                OnPropertyChanged();
            }
        }


        private Command _increaseInvoice;
        public Command IncreaseInvoiceCommand =>
            _increaseInvoice ?? (_increaseInvoice = new Command(() => CurrentInvoice++));

        #region INotifyPropertyChanged implementation

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string name = "") =>
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));

        #endregion
    }
}
