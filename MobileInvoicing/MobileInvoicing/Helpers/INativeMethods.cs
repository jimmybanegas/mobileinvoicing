﻿using System.Threading.Tasks;
using MobileInvoicing.Models;

namespace MobileInvoicing.Helpers
{
    public interface INativeMethods
    {
        Task ShowDialog(InvoiceDetail _item, string _description);

        Task ShowOrderDialog(OrderDetail _item, string _description);


        void SetStatusBar(string _backgroundHexColor);
    }
}
