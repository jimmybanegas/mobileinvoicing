﻿using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MobileInvoicing.Data;
using MobileInvoicing.Models;
using static System.String;

namespace MobileInvoicing.Helpers
{
    public class Utilities
    {
        public static async Task PrintInvoice(Invoice invoice)
        {
            var invoicePrint = new StringBuilder();

            var companyName = IsNullOrEmpty(Settings.CompanyName) ? invoicePrint.Append("") : invoicePrint.Append(Settings.CompanyName + "\n");

            var companyAddress = IsNullOrEmpty(Settings.CompanyAddress) ? invoicePrint.Append("") : invoicePrint.Append(Settings.CompanyAddress + "\n");

            var companyRtn = IsNullOrEmpty(Settings.CompanyRtn) ? invoicePrint.Append("") : invoicePrint.Append("RTN: " + Settings.CompanyRtn + "\n");

            var companePhone = IsNullOrEmpty(Settings.CompanyPhone) ? invoicePrint.Append("") : invoicePrint.Append("Tel: " + Settings.CompanyPhone + "\n");

            var companeEmail = IsNullOrEmpty(Settings.CompanyEmail) ? invoicePrint.Append("") : invoicePrint.Append("Correo: " + Settings.CompanyEmail);

            invoicePrint.Append("\n--------------------------------\n");

            var header = "Factura: " + invoice.InvoiceNumber +
                         "\nCliente: " + invoice.Customer.Name +
                         "\nRTN: " + invoice.Customer.RtnOrId.Trim() +
                         "\nAgente: " + invoice.Vendor.Name.Trim() +
                         "\nFecha: " + invoice.Date.ToString("dd/MM/yyyy HH:mm:ss").Trim();

            invoicePrint.Append(header);

            using (var data = new DataAccess())
            {
                invoice.Products = data.GetInvoiceDetails().Where(c => c.InvoiceId == invoice.Id).ToList();
            }
            invoicePrint.Append("\n--------------------------------\n");
            invoicePrint.Append("Codigo       Descripcion\n");
            invoicePrint.Append("Cant. Precio  Impuesto Total");
            invoicePrint.Append("\n--------------------------------\n");

            var details = Empty;
            foreach (var invoiceProduct in invoice.Products)
            {
                details += "\n" + invoiceProduct.Product.Code.Trim() + " " + invoiceProduct.Product.Name.Trim() +
                           "\n" + invoiceProduct.Quantity + "   " + invoiceProduct.Price.ToString("C").Trim() + "   "
                           + invoiceProduct.Tax.ToString("C").Trim() + "   " + invoiceProduct.Total.ToString("C").Trim();
            }

            invoicePrint.Append(details + "\n");

            invoicePrint.Append("\nSubtotal:          L." + invoice.Subtotal.ToString("C").Trim() + "" +
                                "\nDesctos y rebajas: L." + 0.ToString("C").Trim() +
                                "\nTotal Exonerado:   L." + 0.ToString("C").Trim() +
                                "\nTotal Exento:      L." + invoice.SubTotalNoTax.ToString("C").Trim() +
                                "\nTotal Gravado 15%: L." + invoice.SubTotalTax.ToString("C").Trim() +
                                "\nTotal Gravado 18%: L." + invoice.SubTotalTax2.ToString("C").Trim() +
                                "\nISV 15%:           L." + invoice.Tax.ToString("C").Trim() +
                                "\nISV 18%:           L." + invoice.Tax2.ToString("C").Trim() +
                                "\nTotal:             L." + invoice.Total.ToString("C").Trim());

            invoicePrint.Append("\n================================\n" +
                                "Número de O/C Exenta:" +
                                "\n_____________________" +
                                "\nNo. Const. de Reg. Exoneración:" +
                                "\n_____________________" +
                                "\nNo. Registro SAG:" +
                                "\n_____________________" + "\n\n");

            invoicePrint.Append("\n================================\n" +
                                "CAI: " + invoice.Cai + "" +
                                "\nFact. Inicio: " + invoice.BeginDocument +
                                "\nFact. Final: " + invoice.EndDocument.ToString().Trim() +
                                "\nFecha Limite: " + invoice.LimitDate.ToString("dd/MM/yyyy") +
                                "\n\nOriginal : Cliente " +
                                "\nCopia : Obligado tributario emisor" + "\n\n\n");

            await XF.Bluetooth.Printer.Plugin.XFPrint.Current.PrintText(invoicePrint.ToString(), Settings.PrinterName);
        }

        public static async Task PrintOrder(Order order)
        {
            var invoicePrint = new StringBuilder();
            
            invoicePrint.Append("\n--------------------------------\n");

            var header = "Pedido: " + order.Id +
                         "\nCliente: " + order.Customer.Name +
                         "\nRTN: " + order.Customer.RtnOrId.Trim() +
                         "\nAgente: " + order.Vendor.Name.Trim() +
                         "\nFecha: " + order.Date.ToString("dd/MM/yyyy HH:mm:ss").Trim();

            invoicePrint.Append(header);

            using (var data = new DataAccess())
            {
                order.Products = data.GetOrderDetails().Where(c => c.OrderId == order.Id).ToList();
            }
            invoicePrint.Append("\n--------------------------------\n");
            invoicePrint.Append("Codigo       Descripcion\n");
            invoicePrint.Append("Cant. Precio  Impuesto Total");
            invoicePrint.Append("\n--------------------------------\n");

            var details = Empty;
            foreach (var invoiceProduct in order.Products)
            {
                details += "\n" + invoiceProduct.Product.Code.Trim() + " " + invoiceProduct.Product.Name.Trim() +
                           "\n" + invoiceProduct.Quantity + "   " + invoiceProduct.Price.ToString("C").Trim() + "   "
                           + invoiceProduct.Tax.ToString("C").Trim() + "   " + invoiceProduct.Total.ToString("C").Trim();
            }

            invoicePrint.Append(details + "\n");

            invoicePrint.Append("\nSubtotal:          L." + order.Subtotal.ToString("C").Trim() + "" +
                                "\nDesctos y rebajas: L." + 0.ToString("C").Trim() +
                                "\nTotal Exonerado:   L." + 0.ToString("C").Trim() +
                                "\nTotal Exento:      L." + order.SubTotalNoTax.ToString("C").Trim() +
                                "\nTotal Gravado 15%: L." + order.SubTotalTax.ToString("C").Trim() +
                                "\nTotal Gravado 18%: L." + order.SubTotalTax2.ToString("C").Trim() +
                                "\nISV 15%:           L." + order.Tax.ToString("C").Trim() +
                                "\nISV 18%:           L." + order.Tax2.ToString("C").Trim() +
                                "\nTotal:             L." + order.Total.ToString("C").Trim());

            invoicePrint.Append("\n================================\n" +
                                "\nPEDIDO" + "\n\n\n");

            await XF.Bluetooth.Printer.Plugin.XFPrint.Current.PrintText(invoicePrint.ToString(), Settings.PrinterName);
        }
    }
}
