﻿using Plugin.Connectivity;


namespace MobileInvoicing.Helpers
{
    public class NetworkCheck
    {
        public static bool IsInternet()
        {
           // return false;
            if (CrossConnectivity.Current.IsConnected)
            {
                return true;
            }
            else
            {
                // write your code if there is no Internet available  
                return false;
            }
        }
    }
}
