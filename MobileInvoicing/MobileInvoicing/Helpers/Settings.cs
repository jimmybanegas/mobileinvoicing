// Helpers/Settings.cs

using System;
using Plugin.Settings;
using Plugin.Settings.Abstractions;

namespace MobileInvoicing.Helpers
{
	/// <summary>
	/// This is the Settings static class that can be used in your Core solution or in any
	/// of your client applications. All settings are laid out the same exact way with getters
	/// and setters. 
	/// </summary>
	public static class Settings
	{
		private static ISettings AppSettings
		{
			get
			{
				return CrossSettings.Current;
			}
		}

		#region Setting Constants

		private const string SettingsKey = "settings_key";
		private static readonly string SettingsDefault = string.Empty;

        private const string UsernameKey = "username_key";
        private static readonly string UsernameDefault = "arosoft";

        private const string PasswordKey = "password_key";
        private static readonly string PasswordDefault = string.Empty;

        private const string ServidorKey = "servidor_key";
        private static readonly string ServidorDefault = "190.5.127.182";

        private const string BaseDatosKey = "basedatos_key";
        private static readonly string BaseDatosDefault = "tams_Chicsa";

	    private const string CompanyNameKey = "companyname_key";
	    private static readonly string CompanyNameDefault = String.Empty;

	    private const string CompanyAddressKey = "companyaddress_key";
	    private static readonly string CompanyAddressDefault = String.Empty;

	    private const string CompanyRtnKey = "companyrtn_key";
	    private static readonly string CompanyRtnDefault = String.Empty;

	    private const string CompanyEmailKey = "companyemail_key";
	    private static readonly string CompanyEmailDefault = String.Empty;

	    private const string CompanyPhoneKey = "companyphone_key";
	    private static readonly string CompanyPhoneDefault = String.Empty;

        private const string FechaLimiteKey = "fechalimite_key";
	    private static readonly DateTime FechaLimiteDefault = DateTime.Now;

	    private const string PrinterNameKey = "printername_key";
	    private static readonly string PrinterNameDefault = "BlueTooth Printer";

		private const string PrintInvoiceKey = "printinvoice_key";
		private static readonly string PrintInvoiceDefault = "Si";

		private const string CaiKey = "cai_key";
	    private static readonly string CaiDefault = string.Empty;

        private static readonly int VendorDefault = 0;

        private static readonly int PlaceDefault = 0;

	    private static readonly int PointDefault = 0;

	    private static readonly int BeginDefault = 0;

	    private static readonly int EndDefault = 0;

        private static readonly int CurrentInvoiceDefault = 0;

		#endregion

		public static string GeneralSettings
		{
			get
			{
				return AppSettings.GetValueOrDefault(SettingsKey, SettingsDefault);
			}
			set
			{
				AppSettings.AddOrUpdateValue(SettingsKey, value);
			}
		}

        public static string Username
        {
            get { return AppSettings.GetValueOrDefault(UsernameKey, UsernameDefault); }
            set { AppSettings.AddOrUpdateValue(UsernameKey, value); }
        }

        public static string Password
        {
            get { return AppSettings.GetValueOrDefault(PasswordKey, PasswordDefault); }
            set { AppSettings.AddOrUpdateValue(PasswordKey, value); }
        }

        public static string Servidor
        {
            get { return AppSettings.GetValueOrDefault(ServidorKey, ServidorDefault); }
            set { AppSettings.AddOrUpdateValue(ServidorKey, value); }
        }

        public static string BaseDatos
        {
            get { return AppSettings.GetValueOrDefault(BaseDatosKey, BaseDatosDefault); }
            set { AppSettings.AddOrUpdateValue(BaseDatosKey, value); }
        }

	    public static DateTime FechaLimite
        {
	        get => AppSettings.GetValueOrDefault(nameof(FechaLimite), FechaLimiteDefault);
	        set => AppSettings.AddOrUpdateValue(nameof(FechaLimite), value);
	    }

        public static string CompanyName
        {
            get => AppSettings.GetValueOrDefault(nameof(CompanyNameKey), CompanyNameDefault);
            set => AppSettings.AddOrUpdateValue(nameof(CompanyNameKey), value);
        }

	    public static string CompanyAddress
	    {
	        get => AppSettings.GetValueOrDefault(nameof(CompanyAddressKey), CompanyAddressDefault);
	        set => AppSettings.AddOrUpdateValue(nameof(CompanyAddressKey), value);
	    }

	    public static string CompanyRtn
	    {
	        get => AppSettings.GetValueOrDefault(nameof(CompanyRtnKey), CompanyRtnDefault);
	        set => AppSettings.AddOrUpdateValue(nameof(CompanyRtnKey), value);
	    }

	    public static string CompanyEmail
	    {
	        get => AppSettings.GetValueOrDefault(nameof(CompanyEmailKey), CompanyEmailDefault);
	        set => AppSettings.AddOrUpdateValue(nameof(CompanyEmailKey), value);
	    }

	    public static string CompanyPhone
	    {
	        get => AppSettings.GetValueOrDefault(nameof(CompanyPhoneKey), CompanyPhoneDefault);
	        set => AppSettings.AddOrUpdateValue(nameof(CompanyPhoneKey), value);
	    }

	    public static string PrinterName
	    {
	        get => AppSettings.GetValueOrDefault(nameof(PrinterNameKey), PrinterNameDefault);
	        set => AppSettings.AddOrUpdateValue(nameof(PrinterNameKey), value);
	    }

		public static string PrintInvoice
		{
			get => AppSettings.GetValueOrDefault(nameof(PrintInvoiceKey), PrintInvoiceDefault);
			set => AppSettings.AddOrUpdateValue(nameof(PrintInvoiceKey), value);
		}

		public static int Vendor
        {
	        get => AppSettings.GetValueOrDefault(nameof(Vendor), VendorDefault);
	        set => AppSettings.AddOrUpdateValue(nameof(Vendor), value);
	    }

        public static int Place
	    {
	        get => AppSettings.GetValueOrDefault(nameof(Place), PlaceDefault);
	        set => AppSettings.AddOrUpdateValue(nameof(Place), value);
	    }
        
	    public static int Point
	    {
	        get => AppSettings.GetValueOrDefault(nameof(Point), PointDefault);
	        set => AppSettings.AddOrUpdateValue(nameof(Point), value);
	    }

	    public static int Begin
	    {
	        get => AppSettings.GetValueOrDefault(nameof(Begin), BeginDefault);
	        set => AppSettings.AddOrUpdateValue(nameof(Begin), value);
	    }

	    public static int End
	    {
	        get => AppSettings.GetValueOrDefault(nameof(End), EndDefault);
	        set => AppSettings.AddOrUpdateValue(nameof(End), value);
	    }

        public static int CurrentInvoice
	    {
	        get => AppSettings.GetValueOrDefault(nameof(CurrentInvoice), CurrentInvoiceDefault);
	        set => AppSettings.AddOrUpdateValue(nameof(CurrentInvoice), value);
	    }

        public static string Cai
	    {
	        get { return AppSettings.GetValueOrDefault(CaiKey, CaiDefault); }
	        set { AppSettings.AddOrUpdateValue(CaiKey, value); }
	    }
    }
}